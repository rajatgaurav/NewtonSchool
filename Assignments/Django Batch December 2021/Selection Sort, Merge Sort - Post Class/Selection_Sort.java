/*
Problem Statement
Implement Selection Sort on a given array, and make it sorted.
Input
First line of the input contains an integer, N, which denotes the length of the array. Next N inputs are elements of the array that is to be sorted in ascending order. 

Constraints 
1<=N<=1000 
-10000<=Arr[i]<=10000
Output
Sorted output where each element is space separated
Example
Sample Input: 
3 
3 1 2 

Sample Output: 
1 2 3
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
    private static void selectionSort(int arr[], int n)
    {
        for (int i = 0; i < n-1; i++)
        {
            int minIndex = i;
            for (int j = i+1; j < n; j++)
                if (arr[j] < arr[minIndex])
                    minIndex = j;

            int temp = arr[minIndex];
            arr[minIndex] = arr[i];
            arr[i] = temp;
        }  
    }
 
    private static void printArray(int arr[], int n)
    {
        for (int i=0; i<n; ++i)
            System.out.print(arr[i]+" ");
    }

	public static void main (String[] args) {
                      // Your code here
        Scanner input = new Scanner(System.in);
		int n = input.nextInt();
        int[] arr = new int[n];

        for(int i = 0; i < n; i++){
            arr[i] = input.nextInt();
        }
        selectionSort(arr, n);
        printArray(arr, n);
	}
}