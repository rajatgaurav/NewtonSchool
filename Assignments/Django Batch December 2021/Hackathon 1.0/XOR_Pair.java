/*
Problem Statement
Given an array of positive element having size N and an integer C. Check if there exists a pair (A,B) such that A xor B = C.
Input
First line of input contains number of testcases T. 
The First line of each testcase contains two integers N and C. 
The 2nd line of each testcase, contains N space separated integers denoting the elements of the array A. 

Constraints: 
1 <= T <= 50 
1 <= N <= 10000 
1 <= C <= 10000 
0 <= arr[i] <= 10000
Output
Print "Yes" is the pair exists else print "No" without quotes.(Change line after every answer).
Example
Input: 
2 
7 7 
2 1 10 3 4 9 5 
5 1 
9 9 10 10 3 

Output: 
Yes 
No 

Explanation : 
In first case, pair (2,5) give 7. Hence answer is "Yes". In second case no pair exist such that satisfies the condition hance the answer is "No".
*/


// Python Code

def xorPair(N,C,array):
    from itertools import combinations
    for i in combinations(array,r=2):
        if i[0]^i[1]==C :
            return "Yes"
    return "No"
test_case = int(input())
while test_case>0:
    N,C = list(map(int,input().split()))
    print(xorPair(N,C,list(map(int,input().split()))))
    test_case -=1