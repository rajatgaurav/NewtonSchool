/*
Problem Statement
An array of 5 string is given where each string contains 2 characters, Now you have to sort these strings using insertion sort, like in a dictionary.
Input
Input contains 5 strings of length 2 separated by spaces. 
String contains only uppercase English letters.
Output
Print the sorted array.
Example
INPUT : 
AS KF ER DD JK 

OUTPUT : 
AS DD ER JK KF
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
      Scanner input = new Scanner(System.in);
      String str = input.nextLine();
      String []arr= str.split(" ");
      Arrays.sort(arr);

      for (int i = 0; i < 5; i++) {
        System.out.print(arr[i] + " ");
      }
  }
}