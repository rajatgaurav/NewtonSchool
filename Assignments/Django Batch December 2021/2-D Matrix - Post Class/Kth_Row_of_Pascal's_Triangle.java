/*
Problem Statement
Given an index k, return the k-th row of Pascal’s triangle. 
You must print the k-th row modulo 10^9+7. 

This problem was asked In Google.
Input
The only line of input contains the input k. 

Constraints:- 
1 < = k < = 3000 
Output
Print k-th row of Pascal's Triangle containing k+1 integers modulo 10^9+7.
Example
Sample Input : 
3 

Sample Output: 
1 3 3 1
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
		Scanner input = new Scanner(System.in);
        int k = input.nextInt();

        int[][] pascal = new int[k+1][k+1];
        for(int i=0;i<=k;i++){
            for(int j=0;j<=i;j++){
                if(j==0){
                    pascal[i][j] = 1;
                }
                else{
                    pascal[i][j] = (pascal[i-1][j]%1000000007 + pascal[i-1][j-1]%1000000007)%1000000007;
                }
            }
        }
        for(int j=0;j<=k;j++)
            System.out.print(pascal[k][j]+" ");
	}
}