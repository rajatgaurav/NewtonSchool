/*
Problem Statement
The United States is organizing the presidential election. The number of votes a candidate gets is his/her score. All candidates had distinct scores, which are non-negative integers. 

You know N facts on the candidates' scores. The i-th fact is as follows: the Ai-th highest score among the candidates is Bi. 

Find the maximum possible number of candidates in the election.
Input
First line of input contains a single integer N, the next N lines contain two space-separated integers depicting values A[i] and B[i]. 

Constraints:- 
1 <= N <= 105 
1 <= Ai <= 109 
0 <= Bi <= 109 

Note:- There exists a possible outcome of the election that is consistent with the given facts.
Output
Print the maximum possible number of candidates in the election.
Example
Sample Input:- 
3 
4 7 
2 9 
6 2 

Sample Output:- 
8 

Explanation:- 
The maximum possible number of candidates is achieved when candidates have the following scores: 12, 9, 8, 7, 5, 2, 1, 0.
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int a[] = new int[n];
        int b[] = new int[n];
        for(int i = 0; i < n; i++){
            a[i] = input.nextInt();
            b[i] = input.nextInt();
        }
        int k = b[0];
        int c =a[0];
        
        for(int i=0;i<n;i++){
            if(b[i]<k){
                k = b[i];
                c = a[i];
            }
        }
        System.out.println(k+c);
	}
}