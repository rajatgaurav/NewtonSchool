/*
Problem Statement
How many people must be there in a room to make the probability p such that at- least two people in the room have same birthday?
Input
The first line in the input contains the probability p. 

Constraints 
0 <= p < 1
Output
Print the number of person in the room
Example
Sample Input 
0.7 

Sample Output 
30
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
		Scanner input = new Scanner(System.in);
		double p = input.nextDouble();

		double person = Math.ceil(Math.sqrt(2*365*Math.log(1/(1-p))));
		System.out.print((int)person);  

	}
}