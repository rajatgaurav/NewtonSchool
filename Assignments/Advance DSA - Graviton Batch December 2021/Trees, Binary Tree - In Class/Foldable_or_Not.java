/*
Problem Statement
Given a binary tree, check if the tree can be folded or not. A tree can be folded if left and right subtrees of the tree are mirror images of each other. An empty tree is considered as foldable. 
Consider the below trees: 
(a) and (b) can be folded. 
(c) and (d) cannot be folded.

Input
User Task: 
Since this will be a functional problem. You don't have to take input. You just have to complete the function isFoldable() that takes "root" node as parameter. 

Constraints: 
1 <= T <= 100 
1 <= N <= 10^3 
1 <= node values <= 10^4 

Sum of "N" over all testcases does not exceed 10^5 

For Custom Input: 
First line of input should contains the number of test cases T. For each test case, there will be two lines of input. 
First line contains number of nodes N. Second line will be a string representing the tree as described below: 
The values in the string are in the order of level order traversal of the tree where, numbers denote node values, and a character “N” denotes NULL child. 
Note: If a node has been declared Null using 'N', no information about its children will be given further in the array.
Output
Return "Yes" or "<>bNo" whether the tree is Foldable or not
Example
Sample Input: 
2 
5 
10 7 15 N 9 11 N 
5 
10 7 15 5 N 11 N 

Sample Output: 
Yes 
No 

Explanation: 
Testcase 1: Given tree is 
10 
/ \ 
7 15 
/ \ / \ 
N 9 11 N 
Hence, above tree is structure wise same so it is foldable. 
Testcase 2: Given tree is 
10 
/ \ 
7 15 
/ \ / \ 
5 N 11 N 
Hence, above tree is not structure wise same so it is not foldable.
*/

/*
// Information about the class Node
class Node{
    int data;
    Node left;
    Node right;
    Node(int data){
        this.data = data;
        left=null;
        right=null;
    }
}
*/
static boolean isFoldable(Node node)
{
     // Your code here
    boolean res;
 
        /* base case */
        if (node == null)
            return true;
 
        /* convert left subtree to its mirror */
        mirror(node.left);
 
        /* Compare the structures of the right subtree and mirrored
         left subtree */
        res = isStructSame(node.left, node.right);
 
        /* Get the original tree back */
        mirror(node.left);
 
        return res;
} 

Node root;
 
    static boolean isStructSame(Node a, Node b) {
        if (a == null && b == null)
            return true;
        if (a != null && b != null
            && isStructSame(a.left, b.left)
            && isStructSame(a.right, b.right))
            return true;
 
        return false;
    }
 
    static void mirror(Node node) {
        if (node == null)
            return;
        else {
            Node temp;
 
            /* do the subtrees */
            mirror(node.left);
            mirror(node.right);
 
            /* swap the pointers in this node */
            temp = node.left;
            node.left = node.right;
            node.right = temp;
        }
    }