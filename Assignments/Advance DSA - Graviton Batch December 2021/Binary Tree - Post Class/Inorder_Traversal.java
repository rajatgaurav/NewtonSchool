/*
Problem Statement
Given a binary tree, with root 1, print the inorder traversal of the tree. 
Algorithm Inorder(tree) 
1. Traverse the left subtree 
2. Visit the root. 
3. Traverse the right subtree
Input
The input consists of N+1 lines. 
First line contains the integer N, denoting the number of nodes in the binary tree. 
Next N lines contains two integers denoting the left and right child of the i'th node respectively. 
If the node doesn't have a left or right child, it is denoted by '-1' 

1 <= N <= 100000
Output
Print a single line containing N space separated integers representing the inorder traversal of the given tree
Example
Sample Input 1: 
5 
2 4 
5 3 
-1 -1 
-1 -1 
-1 -1 
Sample output 1: 
5 2 3 1 4 

Explanation: Given binary tree 
1 
/ \ 
2 4 
/ \ 
5 3 
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        Node[] arr = new Node[n];
        for(int i =0; i <n;i++){
            arr[i] = new Node(i + 1);
        }
        for(int i = 0;i<n;i++){
            int left = input.nextInt();
            int right = input.nextInt();
            if(left != -1){
                arr[i].left = arr[left -1];
            }
            if(right != -1){
                arr[i].right = arr[right - 1];
            }
        }
        printInorder(arr[0]);
	}

    static Node root;
 
    Main() { root = null; }
 
    /* Given a binary tree, print its nodes in inorder*/
    static void printInorder(Node node)
    {
        if (node == null)
            return;
 
        /* first recur on left child */
        printInorder(node.left);
 
        /* then print the data of node */
        System.out.print(node.key + " ");
 
        /* now recur on right child */
        printInorder(node.right);
    }
 
    // Wrappers over above recursive functions
    static void printInorder() { printInorder(root); }
}

class Node {
    int key;
    Node left, right;
 
    public Node(int item)
    {
        key = item;
        left = right = null;
    }
}