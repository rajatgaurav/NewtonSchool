/*
Problem Statement
You think that integers divisible by K are nice. Given L and R (L<=R), find the number of nice integers from L to R (both inclusive).
Input
First Line of input contains three integers L R K 

Constraints : 
0 <= L <= R <= 1000000000000000000(10^18) 
1 <= K <= 1000000000000000000(10^18)
Output
Output a single integer which is the number of nice integers from L to R (both inclusive).
Example
Sample input 1 
1 10 2 

Sample output 1 
5 

Sample intput 2 
4 5 3 

Sample output 2 
0 

Explanation:- 
Required divisors = 2 4 6 8 10
*/


//c++ Code 

#include <bits/stdc++.h> // header file includes every Standard library
using namespace std;
int main() {
    // Your code here
    long long l,r,k;
    cin>>l>>r>>k;
    if(l%k==0)
        cout<<((r/k)-(l/k))+1;
    else
        cout<<(r/k)-(l/k);
    return 0;
}