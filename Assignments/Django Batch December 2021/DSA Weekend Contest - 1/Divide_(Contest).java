/*
Problem Statement
Given an integer N and M check if N candies can be divided in M people such that each person get equal number of candies.
Input
Input contains two integers N and M. 

Constraints: 
1 <= N <= 10^18 
1 <= M <= 10^18
Output
Print "Yes" if it is possible otherwise "No".
Example
Sample Input 
10 5 

Sample Output 
Yes 

Explanation: Give 2 candies to all. 

Sample Input: 
4 3 

Sample Output: 
No
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework
// don't change the name of this class
// you can add inner classes if needed
class Main {
    public static void main (String[] args) {
                      // Your code here
        Scanner input  = new Scanner(System.in);
        long n = input.nextLong();
        long m = input.nextLong();
        if(n % m == 0){
            System.out.println("Yes");
        }else{
            System.out.println("No");
        }
    }
}

