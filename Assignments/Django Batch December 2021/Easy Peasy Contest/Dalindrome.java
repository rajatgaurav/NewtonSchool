/*
Problem Statement
Everyone has heard of palindromes, right! A palindrome is a string that remains the same if reversed. 
Let's define a new term, Dalindrome. 
A Dalindrome is a string whose atleast one of the substrings is a palindrome. 
Given a string, find whether it's a Dalindrome.
Input
The only line of input contains a string to be checked. 

Constraints 
1 <= length of string <= 100
Output
Output "Yes" if string is a Dalindrome, else output "No".
Example
Sample Input 
cbabcc 

Sample Output 
Yes 

Explanation: "bab" is one of the substrings of the string that is a palindrome. There may be other substrings that are palindrome as well like "cc", or "cbabc". The question requires atleast one.
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework
public class Main {
    public static int findDalindrome(String input, int j, int k) {
        int count = 0;
        for (; j >= 0 && k < input.length(); --j, ++k) {
            if (input.charAt(j) != input.charAt(k)) {      
            break;
            } 
            count++;
        }
        return count;
    }
    public static int findAllDalindrome(String input) {
        int count = 0;
        if(input.length() == 1) return ++count;
        for(int i = 0 ; i < input.length() ; ++i) {
            count+= findDalindrome(input, i-1, i+1);
            count+= findDalindrome(input, i, i+1);
        }
        return count;
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String str = input.nextLine();
        int count = findAllDalindrome(str);
        if(count > 0)
            System.out.println("Yes");
        else
            System.out.println("No");
    }
}