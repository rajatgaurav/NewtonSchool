/*
Problem Statement
Given an array, sort the array in reverse order by simply swapping its adjacent elements.
Input
First line of the input contains an integer, N, which denotes the length of the array. Next N inputs are elements of the array that is to be sorted in descending order. 

Constraints 
1<=N<=1000 
-10000<=Arr[i]<=100000
Output
Output sorted array in descending order where each element is space separated.
Example
Sample Input: 
6 
3 1 2 7 9 87 

Sample Output: 
87 9 7 3 2 1
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	static void bubbleSort(int a[]) {
		int i, k, n = a.length;
		int t;
		for (i = 0; i < n / 2; i++) {
			t = a[i];
			a[i] = a[n - i - 1];
			a[n - i - 1] = t;
		}
	}

	public static void main (String[] args) {
                      // Your code here
		Scanner input = new Scanner(System.in);
		int t = input.nextInt();
		int[]arr = new int[t];

		for(int i = 0; i < t; i++){
			arr[i] = input.nextInt();
		}
		int n = arr.length;

		Arrays.sort(arr);
		bubbleSort(arr);

		for (int i = 0; i < n; ++i) {
			System.out.print(arr[i] + " ");
		}
	}
}