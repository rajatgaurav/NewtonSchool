/*
Problem Statement
You're given a string S of lowercase letters of the english alphabet. Find whether you can choose some characters of the string S in any order to create the string "red".
Input
The first and the only line of input contains the string S. 

Constraints 
1 <= |S| <= 100 
All the characters in S are lowercase letters of the english alphabet.
Output
Output "Yes" (without quotes) if you can create the string "red", else output "No" (without quotes).
Example
Sample Input 
damngrey 

Sample Output 
Yes 

Explanation: We choose character at position 6, then position 7, then position 1. 

Sample Input 
newtonschool 

Sample Output 
No
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework
// don't change the name of this class
// you can add inner classes if needed
class Main {
    public static void main (String[] args) {
                      // Your code here
        Scanner input = new Scanner(System.in);
        String n = input.next();
        // for(int i =0;i<n.length();i++){
            if(n.contains("r") && n.contains("e") && n.contains("d")){
                System.out.println("Yes");
            } else {
                System.out.println("No");
            }
    }
}