/*
Problem Statement
Given a BST containing N nodes. The task is to find the minimum absolute difference possible between any two different nodes.
Input
User Task: 
Since this will be a functional problem you don't have to take input. You just have to complete the function absMinDist() that takes "root" node as parameter 

Constraints: 
1 <= T <= 100 
1 <= N <= 10^5 
1 <= node values <= 10^5 

Sum of N over all testcases does not exceed 10^6 

Output
For each testcase you need to return the minimum absolute difference between any two different nodes.The driver code will take care of printing new line.
Example
Input: 
2 
5 3 6 
5 N 9 8 N 

Output: 
1 
1 

Explanation: 
Testcase 1: 
5 
/ \ 
3 6 
the minimum abs difference between two nodes i.e 5 & 6 is 1. 

Testcase 2: 
5 
/ \ 
N 9 
/ \ 
8 N 
the minimum absolute difference between two nodes i.e. 9 & 8 is 1.
*/

/*
// Information about the class Node
class Node{
    int data;
    Node left;
    Node right;
  
    Node(int data){
        this.data = data;
        left=null;
        right=null;
       
    }
}
*/

static Node prev;
static int ans;

public static  int absMinDist(Node root) {
        // Your code here
    prev = null;
    ans = Integer.MAX_VALUE;

    inorder(root);

    return ans;
}

static void inorder(Node curr) {
    if (curr == null)
        return;
    inorder(curr.left);
    
    if (prev != null)
        ans = Math.min(curr.data - prev.data, ans);
    prev = curr;
    inorder(curr.right);
}