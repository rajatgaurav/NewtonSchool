/*
Problem Statement
Alongside acting as his personal assistant F. R. I. D. A. Y also manages the Avengers Tower. With all the heroes of the universe arriving at the place it becomes quite messy to cater to the needs of all the guests. Assuming every super hero arrives by a train, how many platforms are needed on the Avengers tower to ensure no train gets delayed. Given arrival and departure times of all trains that reach Avengers Tower. Consider that all the trains arrive on the same day and leave on the same day. Arrival and departure time can never be the same for a train but we can have the arrival time of one train equal to the departure time of the other. At any given instance of time, the same platform cannot be used for both departure of a train and arrival of another train. In such cases, we need different platforms.
Input
The first line of input has the number of visitors. 
It is followed by the entry of two books in the form of two arrays. 
It is not mentioned which Array contains entry time and which has exit time but it is sure all entry time is in one array and all exit time is in other array. 
eg: 9:00 is written as 900. 
if ab:cd is the time then it will be written as ab*100 + cd. 

Constraints:- 
1 ≤ n ≤ 10000
Output
The output contains a single Line the minimum number of platforms needed.
Example
Sample Input:- 
6 
900 940 950 1100 1500 1800 
910 1200 1120 1130 1900 2000 

Sample Output:- 
3 

Sample Input:- 
3 
0900 1235 1100 
1000 1200 1240 

Sample Output:- 
1
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	static int differentPlatforms(int arrivalTime[], int departureTime[], int visitors) {
		// Sort arrival and departure arrays
		Arrays.sort(arrivalTime);
		Arrays.sort(departureTime);

		// platformNeeded indicates number of platforms
		// needed at a time
		int platformNeeded = 1, result = 1;
		int i = 1, j = 0;

		// Similar to merge in merge sort to process
		// all events in sorted order
		while (i < visitors && j < visitors) {
			// If next event in sorted order is arrival,
			// increment count of platforms needed
			if (arrivalTime[i] <= departureTime[j]) {
				platformNeeded++;
				i++;
			}

			// Else decrement count of platforms needed
			else if (arrivalTime[i] > departureTime[j]) {
				platformNeeded--;
				j++;
			}

			// Update result if needed
			if (platformNeeded > result)
				result = platformNeeded;
		}
		return result;
	}

	public static void main (String[] args) {
                      // Your code here
        Scanner input = new Scanner(System.in);
        int visitors = input.nextInt();
        int[] arrivalTime = new int[visitors];
        int[] departureTime = new int[visitors];

        for(int i = 0; i < visitors; i++) {
            arrivalTime[i] = input.nextInt();
        }
		for(int i = 0; i < visitors; i++) {
            departureTime[i] = input.nextInt();
        }
		if(arrivalTime[0]<departureTime[0]){
			System.out.print(differentPlatforms(arrivalTime, departureTime, visitors));
		}else{
			System.out.print(differentPlatforms(departureTime, arrivalTime, visitors));
		}
        
	}
}