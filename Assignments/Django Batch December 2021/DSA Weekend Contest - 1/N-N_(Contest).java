/*
Problem Statement
Given an integer N, find its Nth smallest non negative integer that is divisible by N.
Input
The first and the only line of input contains an integer N. 

Constraints 
1 <= N <= 1000
Output
Output a single integer, the Nth smallest non negative multiple of N.
Example
Sample Input 
5 

Sample Output 
20 

Explanation: The 5th smallest integer divisible by 5 is 20. The smaller integers divisible by 5 are 0, 5, 10, and 15. 

Sample Input 
2 

Sample Output 
2
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
        Scanner input = new Scanner(System.in);
		int n = input.nextInt();
		int x = 0;
		int i = 0;
		while(i < n ){
			x = n * i;
			i++;
		}
		System.out.print(x);
	}
}