/*
Problem Statement
Given a binary tree, count the number of leaves in it. A node having no child node is called a leaf.
Input
The first line contains the integer N, denoting the number of nodes in the binary tree. 
Next N lines contain two integers, denoting the left and right child of the i-th node respectively. 
If the node doesn't have a left or right child, it is denoted by '-1' 

1 <= N <= 100000
Output
Print the number of leaves in the binary tree
Example
Sample Input 1: 
7 
2 4 
5 3 
-1 -1 
-1 7 
6 -1 
-1 -1 
-1 -1 

Sample output 1: 
3 

Explanation: Given binary tree 
1 
/ \ 
2 4 
/ \ \ 
5 3 7 
/ 
6 
Node 3, 6, 7 are the leaves here
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        Node[] arr = new Node[n];
        for(int i =0; i <n;i++){
            arr[i] = new Node(i + 1);
        }
        for(int i = 0;i<n;i++){
            int left = input.nextInt();
            int right = input.nextInt();
            if(left != -1){
                arr[i].left = arr[left -1];
            }
            if(right != -1){
                arr[i].right = arr[right - 1];
            }
        }
        System.out.println(countLeafNode(arr[0]));
	}

    private static int countLeafNode(Node root){
        if(root == null){
            return 0;
        }
        if(root.left == null && root.right == null){
            return 1;
        }
        int leafNodesOnLeft = countLeafNode(root.left);
        int leafNodesOnRight = countLeafNode(root.right);
        return leafNodesOnLeft + leafNodesOnRight;
    }

    private static void preOrder(Node root){
        if(root == null){
            return;
        }
        System.out.print(root.data + " ");
        preOrder(root.left);
        preOrder(root.right);
    }

     private static void inOrder(Node root){
        if(root == null){
            return;
        }
        inOrder(root.left);
        System.out.print(root.data + " ");
        inOrder(root.right);
    }
}

class Node {
    public int data;
    public Node left;
    public Node right;

    public Node(int data){
        this.data = data;
    }
}