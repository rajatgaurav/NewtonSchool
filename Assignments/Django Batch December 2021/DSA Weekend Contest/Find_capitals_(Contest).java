/*
Problem Statement
Given a string S (containing only letters of the English alphabet), find all the capital letters in it (in exactly same order as the string) 

Note:- It is guaranteed that the given string will contain at least one capital.
Input
Input contains a single line containing the string S. 

Constraints:- 
1 <= |S| <= 100
Output
Print all capital letters present in the string separated by space, in the order of their appearance in the main string.
Example
Sample input:- 
AbcdEF 

Sample Output:- 
A E F 

Sample Input:- 
NewtonSchool 

Sample Output:- 
N S
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework
// don't change the name of this class
// you can add inner classes if needed
class Main {
    public static void main (String[] args) {
                      // Your code here
            Scanner input = new Scanner(System.in);
            String str = input.nextLine();
            for(int i = 0; i < str.length(); i++){
                if((int)(str.charAt(i)) >= 65 && (int)(str.charAt(i)) <= 90){
                    System.out.print(str.charAt(i) + " ");
                }
            }
    }
}