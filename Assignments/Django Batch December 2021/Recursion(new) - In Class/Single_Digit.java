/*
Problem Statement
Given a number N, your task is to convert the number into a single digit by repeatedly replacing the number N with its sum of digits until the number becomes a single digit.
Input
The input contains a single integer N. 

Constraints:- 
1 <= N <= 100000
Output
Print the single digit after performing the given operation.
Example
Sample Input:- 
987 

Sample Output:- 
6 

Explanation:- 
987 - > 9 + 8 + 7 => 24 = 2 + 4 => 6 

Sample Input:- 
91 

Sample Output:- 
1
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	static int singleSum(int n) {
		int sum = 0;

		while (n > 0 || sum > 9) {
			if (n == 0) {
				n = sum;
				sum = 0;
			}
			sum += n % 10;
			n /= 10;
		}
		return sum;
	}
	public static void main (String[] args) {
                      // Your code here
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();

		System.out.println(singleSum(n));
	}
}