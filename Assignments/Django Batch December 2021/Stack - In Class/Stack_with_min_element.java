/*
Given a stack of integers and N queries. Your task is to perform these operations:- 
push:- this operation will add an element to your current stack. 
pop:- remove the element that is on top 
top:- print the element which is currently on top of stack 
getMin:- print the minimum element from the stack 

Note:- if stack is already empty than pop operation will do nothing,-1 will printed as minimum element and 0 will be printed as a top element of stack if it is empty.
Input
User task: 
Since this will be a functional problem, you don't have to take input. You just have to complete the functions: 
push():- that takes the stack and the integer to be added as a parameter. 
pop():- that takes the stack as parameter. 
top() :- that takes the stack as parameter. 
getMin() that takes the stack as parameter. 

Constraints: 
1 <= N <= 500000
Output
You don't need to print anything else other than in the getMin() function in which you need to print the minimum element of the stack, or -1 if the stack is empty and in the top() function in which need to print the top most element of your stack or 0 if the stack is empty. 

Note:- Each output is to be printed in a new line.
Example
Sample Input:- 
6 
push 3 
push 5 
getMin 
top 
pop 
top 

Sample Output:- 
3 
5 
3
*/

//Function to insert element to stack
public static void push (Stack < Integer > s, int x) 
{
    Integer minEle = 0;
//Enter your code here
    if (s.isEmpty())
        {
            minEle = x;
            s.push(x);
            //System.out.println(x);
            return;
        }
 
        // If new number is less than original minEle
        if (x < minEle)
        {
            s.push(2*x - minEle);
            minEle = x;
        }
 
        else
            s.push(x);
 
        //System.out.println(x);
    
} 
 
    // Function to pop element from stack
public static void pop (Stack < Integer > s) 
{
    Integer minEle = 0;
//Enter your code here
    if (s.isEmpty())
        {
            System.out.println("Stack is empty");
            //return;
        }
 
        //System.out.print("Top Most Element Removed: ");
        Integer t = s.pop();
 
        // Minimum will change as the minimum element
        // of the stack is being removed.
        if (t < minEle)
        {
            System.out.println(minEle);
            minEle = 2*minEle - t;
        }
 
        else
            System.out.print("");
} 
 
    // Function to print the top of stack
 public static void top(Stack < Integer > s) 
{
    Integer minEle = 0;
 //Enter your code here
    if (s.isEmpty())
        {
            System.out.println("0");
            return;
        }
 
        Integer t = s.peek(); // Top element.
 
        //System.out.print("Top Most Element is: ");
 
        // If t < minEle means minEle stores
        // value of t.
        if (t < minEle)
            System.out.println(minEle);
        else
            System.out.println(t);
 }

//Function to print the minimum element from stack
public static void getMin(Stack<Integer> s)
{
    Integer minEle = 0;
//Enter your code here
    if (s.isEmpty())
            System.out.println("0");
 
        // variable minEle stores the minimum element
        // in the stack.
        else
            System.out.println(minEle);
}