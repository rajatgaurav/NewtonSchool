/*
Problem Statement
For a number N we have N! unique permutations. Your task is to find the Kth smallest permutation when all N! permutations are arranged in the sorted order.
Input
Input contains only two integers, the value of N and K. 

Constraints:- 
1 <= N <= 10000 
1 <= K <= min(N!,100000000)
Output
Print the Kth permutation in form of a string. i. e don't print spaces between two numbers.
Example
Sample Input:- 
3 5 

Sample Output:- 
312 

Explanation:- 
All permutations of length 3 are:- 
123 
132 
213 
232 
312 
321 

Sample Input:- 
11 2 

Sample Output:- 
1234567891110
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
		Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int k = input.nextInt();

        int[] arr = new int[n];
        for(int i =0; i<n ; i++){
            arr[i] = i+1;
        }
        
        while(k > 1) {
            for (int i = n - 1; i > 0; i--) {
                if (arr[i - 1] < arr[i]) {
                    int j = n-1;
                    while(j>= i) {
                        if(arr[j] > arr[i-1]) {
                            swap(arr, i - 1, j);
                            break;
                        }
                        j--;
                    }
                    reverse(arr, i, n - 1);
                    break;
                }
            }
            k--;
        }
        for (int i = 0; i < n; i++) {
            System.out.print(arr[i]);
        }
	}
	private static void swap(int[] arr, int a, int b){
        int temp = arr[a];
        arr[a] = arr[b];
        arr[b] = temp;
    }
    private static void reverse(int[] arr, int low, int high){
        if(low >= high){
            return;
        }
        swap(arr, low, high);
        reverse(arr, low+1, high-1);
    }
}