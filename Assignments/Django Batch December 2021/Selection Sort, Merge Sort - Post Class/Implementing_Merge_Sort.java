/*
Problem Statement
Given an unsorted array, your task is to sort the array using merge sort.
Input
User Task: 
Since this will be a functional problem, you don't have to take input. You just have to complete the function implementMergeSort() that takes 3 arguments. 
arr: input array 
start: starting index which is 0 
end: ending index of array 

Constraints 
1 <= T <= 100 
1 <= N <= 10^4 
1 <= Arr[i] <= 10^5 

Sum of 'N' over all test cases does not exceed 10^6
Output
You need to return the sorted array. The driver code will print the array in sorted form.
Example
Sample Input: 
2 
3 
3 1 2 
3 
4 5 6 

Sample Output: 
1 2 3 
4 5 6
*/

public static int[] implementMergeSort(int arr[], int start, int end)
{
        // Your code here
       // You can have your own function where you will use 
      // start and end position for divide purpose
        
    if(start < end) {
	int mid = (start + end) / 2;
	implementMergeSort(arr, start, mid);
	implementMergeSort(arr, mid+1, end);
	mergeSort(arr, start, mid, end);
    }
    return arr;
} 

public static void mergeSort(int arr[], int start, int mid, int end) {

	int temp[] = new int[end - start + 1];
	int i = start, j = mid+1, k = 0;

	while(i <= mid && j <= end) {
		if(arr[i] <= arr[j]) {
			temp[k] = arr[i];
			k += 1; i += 1;
		}
		else {
			temp[k] = arr[j];
			k += 1; j += 1;
		}
	}

	while(i <= mid) {
		temp[k] = arr[i];
		k += 1; i += 1;
	}

	while(j <= end) {
		temp[k] = arr[j];
		k += 1; j += 1;
	}

	for(i = start; i <= end; i += 1) {
		arr[i] = temp[i - start];
	}
}