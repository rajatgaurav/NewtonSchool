/*
Problem Statement
Given a string S. Find a string R which is an anagram of S and the hamming distance between S and R is maximum. 
An anagram of a string is another string that contains the same characters, only the order of characters can be different. 
Hamming distance between two strings of equal length is the number of positions at which the corresponding character is different.
Input
The first and the only line of input contains a single string S. 

Constraints: 
1 <= |S| <= 100000 
S contains only lowercase letters of the English alphabet.
Output
Print the maximum hamming distance between S and R.
Example
Sample Input 1 
absba 

Sample Output 1 
5 

Explanation: R can be "bsaab" which has hamming distance of 5 from S. 

Sample Input 2 
aaa 

Sample Output 2 
0 

Explanation: R can be "aaa" which has hamming distance of 0 from S.
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework
// don't change the name of this class
// you can add inner classes if needed
class Main {
    public static void main (String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String s = br.readLine();
        int[] arr = new int[27];
        int total_sum = s.length();
        for (int i = 0;i<s.length();i++) {
            arr[(int)(s.charAt(i))-97]++;
        }
        int count = 0;
        int diff = 0;
        for (int i = 0; i < 27; i++) {
            diff = total_sum - arr[i];
            if (arr[i] != 0 && diff >= arr[i]) {
                count+= arr[i];
            }
            else if ( arr[i] != 0 && diff < arr[i]) {
                count+=diff;
            }
        }
        System.out.println(count);
    }
}