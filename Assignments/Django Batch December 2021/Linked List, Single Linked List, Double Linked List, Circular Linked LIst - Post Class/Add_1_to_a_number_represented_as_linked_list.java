/*
Problem Statement
A number (n) is represented in Linked List such that each digit corresponds to a node in linked list. Add 1 to it. 

Note:- Linked list representation of a number is from left to right i.e if the number is 123 than in linked list it is represented as 3->2->1
Input
User Task: 
Since this will be a functional problem, you don't have to take input. You just have to complete the function addOne() that takes head node of the linked list as parameter. 

Constraints: 
1 <=length of n<= 1000
Output
Return the head of the modified linked list.
Example
Input 1: 
456 

Output 1: 
457 

Input 2: 
999 

Output 2: 
1000
*/

/*
class Node {
    Node next;
    int data;
    Node(int data) {
        this.data = data;
        next = null;
    }
}
*/

//   static Node addOne(Node head){
// // return the head of the modified linked list
// }

/*
class Node {
    Node next;
    int data;
    Node(int data) {
        this.data = data;
        next = null;
    }
}
*/
public static Node addOne(Node l1) {
    Node l2=new Node(1);
 Node dummyHead = new Node(0);
 Node p = l1, q = l2, curr = dummyHead;
 int carry = 0;
 while (p != null || q != null) {
     int x = (p != null) ? p.data : 0;
     int y = (q != null) ? q.data: 0;
     int sum = carry + x + y;
     carry = sum / 10;
     curr.next = new Node(sum % 10);
     curr = curr.next;
     if (p != null) p = p.next;
     if (q != null) q = q.next;
 }
 if (carry > 0) {
     curr.next = new Node(carry);
 }
 return dummyHead.next;
}