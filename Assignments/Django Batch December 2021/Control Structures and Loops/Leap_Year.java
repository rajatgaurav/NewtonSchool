/*
Problem Statement
Given a year, find if it is a leap year. Leap year is the year that is multiple of 4. But, multiples of 100 which are not multiples of 400 are not leap years.
Input
User Task: 
Complete the function LeapYear() that takes integer n as a parameter. 

Constraint: 
1 <= n <= 5000
Output
If it is a leap year then print YES and if it is not a leap year, then print NO
Example
Sample Input: 
2000 

Sample Output: 
YES 

Sample Input: 
2003 

Sample Output: 
NO 

Sample Input: 
1900 

Sample Output: 
NO
*/

import java.util.Scanner;

class Main {
   public static void main (String[] args) { 
       Scanner input = new Scanner(System.in);
       int n = input.nextInt();
       LeapYear(n);
    }

    static void LeapYear(int year){
        if(((year % 4 ==0) && (year % 100 !=0)) || (year % 400==0)){
            System.out.print("YES");
        } else {
            System.out.print("NO");
        }
    }
}