/*
Problem Statement
You are given three distinct positive integers, A, B and C, in the input. Your task is to find their median. 

The median of a set of integers is the number at the middle position if they are arranged in ascending order.
Input
The input consists of a single line containing three integers A, B and C. 

Constraints: 
1 ≤ A, B, C ≤ 109 
A, B, C are pairwise distinct. 
Output
Print a single integer, the median of the given numbers.
Example
Sample Input 1: 
5 1 3 

Sample Output 1: 
3 

Sample Input 2: 
1 2 3 

Sample Output 2: 
2
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework
// don't change the name of this class
// you can add inner classes if needed
class Main {
    public static void main (String[] args) throws Exception {
                      // Your code here
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String[] str = br.readLine().trim().split(" ");
            int[] arr = new int[str.length];
            for(int i = 0; i < str.length; i++){
                arr[i] = Integer.parseInt(str[i]);
            }
            Arrays.sort(arr);
            System.out.println(arr[arr.length / 2]);
    }
}