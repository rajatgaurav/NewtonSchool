/*
Problem Statement
You are N integers A[1], A[2],. , A[N]. You need to find whether their product is divisible by 4.
Input
The first line of input contains a single integer N. 
The next line contains N singly spaced integers, A[1], A[2],. , A[N]. 

Constraints 
1 <= N <= 10 
1 <= A[i] <= 1000000000
Output
Output "YES" if the product is divisible by 4, else output "NO". (without quotes)
Example
Sample Input 
5 
3 5 2 5 1 

Sample Output 
NO 

Explanation: Product = 150 and 150 is not divisible by 4. 

Sample Input 
4 
1 3 8 2 

Sample Output 
YES 

Explanation: Product = 48 and 48 is divisible by 4.
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
		Scanner input = new Scanner(System.in);
		int n = input.nextInt();
		long product = 1;
		int[] arr = new int[n];
		if(n >= 1 && n <= 10) {
		for(int i = 0; i < n; i++) {
			arr[i] = input.nextInt();
			product *= arr[i];
		}
		if(product % 4 == 0)
			System.out.print("YES");
		else
			System.out.print("NO");}
	}
}