/*
Problem Statement
Given a number N, you need to check whether the given number is Palindrome or not. A number is said to be Palindrome when it reads the same from backward as forward.
Input
User task: 
Since this is a functional problem you don't have to worry about the input. You just have to complete the function isPalindrome() which contains N as a parameter. 

Constraints: 
1 <= N <= 9999
Output
You need to return "true" is the number is palindrome otherwise "false".
Example
Sample Input: 
5 

Sample Output: 
true 

Sample Input: 
121 

Sample Output: 
true
*/

static boolean isPalindrome(int N)
    {
       // your code here
       int divisor = 1;
        while (N / divisor >= 10)
            divisor *= 10;
      
        while (N != 0)
        {
            int leading = N / divisor;
            int trailing = N % 10;
   
            if (leading != trailing) 
                return false;
      
            N = (N % divisor) / 10;
            divisor = divisor / 100;
        }
        return true;
    }


/**
2nd logic

int temp = N;
       int result = 0;
       while(temp > 0){
          int digit = temp%10;
          result = (result*10) + digit;
          temp = temp/10;
       }
       if(result == N){
          return true;
       }
       return false;

*/