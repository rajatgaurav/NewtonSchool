/*
Problem Statement
Given an array of N elements. In the array, each element is present twice except for 1 element whose frequency in the array is 1. Hence the length of the array will always be odd. 
Find the unique number.
Input
An integer, N, representing the size of the array. In the next line, N space-separated integers follow. 

Constraints: 
1 <= N <=105 
1 <= A[i] <=108
Output
Output the element with frequency 1.
Example
Input : 
5 
1 1 2 2 3 

Output: 
3
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
    static int singleelement(int arr[], int n) {
	    int low = 0, high = n - 2;
	    int mid;
	
	    while (low <= high) {
		    mid = (low + high) / 2;
		    if (arr[mid] == arr[mid ^ 1]) {
			    low = mid + 1;
		    }
		    else {
			    high = mid - 1;
		    }
	    }
	    return arr[low];
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[] arr = new int[n];

        for(int i = 0; i < n; i++) {
            arr[i] = input.nextInt();
        }

	    Arrays.sort(arr);
	
	    System.out.print(singleelement(arr, n));
    }

	/*static int findSingle(int ar[], int ar_size) {
		// Do XOR of all elements and return
		int res = ar[0];
		for (int i = 1; i < ar_size; i++)
			res = res ^ ar[i];
	
		return res;
	}

	public static void main (String[] args) {
                      // Your code here
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[] arr = new int[n];

        for(int i = 0; i < n; i++) {
            arr[i] = input.nextInt();
        }
        System.out.print(findSingle(arr, n) + " ");
	}*/
}