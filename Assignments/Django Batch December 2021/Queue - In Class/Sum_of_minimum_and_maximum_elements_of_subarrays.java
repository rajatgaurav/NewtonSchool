/*
Problem Statement
Given an array of size N, your task is to calculate the total sum of maximum and minimum elements in each subarray of size K. 
See example for better understanding.
Input
First line of input contains an two space separated integers depicting values of N and K, next line contains N space separated integers depicting values of Arr[i]. 

Constraints:- 
1 < = k < = N < = 100000 
-100000 < = Arr[i] < = 100000
Output
Print the required sum
Example
Sample Input:- 
5 3 
1 2 3 4 5 

Sample Output:- 
18 

Explanation:- 
For subarray 1 2 3 :- 1 + 3 = 4 
For subarray 2 3 4 :- 2 + 4 = 6 
For subarray 3 4 5 :- 3 + 5 = 8 
total sum = 4+6+8 = 18 

Sample Input:- 
7 4 
2 5 -1 7 -3 -1 -2 

Sample Output:- 
18
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static long sumOfMinAndMaxElementsOfSubarrays(int arr[] , int k) {
		long sum = 0;

		Deque<Integer> maxQue = new LinkedList<>(), minQue = new LinkedList<>();

		for (int i = 0; i < k; i++) {
			while ( !maxQue.isEmpty() && arr[maxQue.peekLast()] >= arr[i]) {
				maxQue.removeLast();
			}
			maxQue.addLast(i);	

			while ( !minQue.isEmpty() && arr[minQue.peekLast()] <= arr[i]) {
				minQue.removeLast();
			}
			minQue.addLast(i);	
		}

		for (int i = k ; i < arr.length; i++ ) {
			sum += arr[maxQue.peekFirst()] + arr[minQue.peekFirst()];
			while ( !maxQue.isEmpty() && maxQue.peekFirst() <= i - k)
				maxQue.removeFirst();
			while ( !minQue.isEmpty() && minQue.peekFirst() <= i - k)
				minQue.removeFirst();

			while ( !maxQue.isEmpty() && arr[maxQue.peekLast()] >= arr[i]) {
				maxQue.removeLast();
			}
			maxQue.addLast(i);	

			while ( !minQue.isEmpty() && arr[minQue.peekLast()] <= arr[i]) {
				minQue.removeLast();
			}
			minQue.addLast(i);
		}

		sum += arr[maxQue.peekFirst()] + arr[minQue.peekFirst()];

		return sum;
	}

	public static void main (String[] args) {
                      // Your code here
		Scanner input = new Scanner(System.in);
		int n = input.nextInt();
		int k = input.nextInt();
		int[] arr = new int[n];
		for (int i = 0; i < n; i++) {
			arr[i] = input.nextInt();
		}
		System.out.print(sumOfMinAndMaxElementsOfSubarrays(arr, k));
	}
}