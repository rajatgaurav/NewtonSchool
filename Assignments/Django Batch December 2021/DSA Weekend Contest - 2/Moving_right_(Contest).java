/*
Problem Statement
Given an array of heights of N buildings in a row. You can start from any building and jump to the adjacent right building till the height of building to the right is less than or equal to the height of your current building. Find the maximum number of jumps you can make.
Input
First line of input contains a single integer N. 
Second line of input contains N integers, denoting the array height. 

Constraints: 
1 <= N <= 100000 
1 <= height[i] <= 1000000000
Output
Print the maximum number of jumps you can make.
Example
Sample Input 
5 
5 4 1 2 1 

Sample Output 
2 

Explanation: We start from building with height 5 then jump right to building with height 4 then again to building with height 1 making total 2 jumps.
*/


// C++ Code

#include <bits/stdc++.h> // header file includes every Standard library
using namespace std;
int main() {
   long long N,count=0,k;
   cin>>N;
   long long arr[N],maxi=0;
   for(long long i=0;i<N;i++){
       cin>>arr[i];
   }
    for(int i=0;i<N;i++){
        if(arr[i+1]<=arr[i]){
            count++;
        }
        else{
            count=0;
        }
        if(count>maxi){
            maxi=count;
        }
   }
   cout<<maxi<<endl;
}