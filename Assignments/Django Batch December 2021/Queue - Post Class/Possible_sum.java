/*
Problem Statement
Given two binary trees, your task is to check if their exist a pair of nodes one from each tree such that their sum is equal to a specified target.
Input
User Task: 
Since this will be a functional problem, you don't have to take input. You just have to complete the function pairsum() that takes root nodes of both trees and the target value as parameters. 

Constraints: 
1 < = Nodes < = 10000 
1 < = Node.data < = 10000 
1 < = target < = 20000
Output
Return true if their exist a pair of nodes one from each tree such that their sum is equal to a specified target else return false.
Example
Sample Input:- 
1 5 
/ \ \ 
2 3 6 

target = 8 

Sample Output:- 
1
*/

static boolean pairsum(Node root1, Node root2, int target){
    //Enter your code here
        if (root1 == null || root2 == null)
            return false;
          
            // stack 'st1' used for the inorder
            // traversal of BST 1
            // stack 'st2' used for the reverse
            // inorder traversal of BST 2
            //stack<Node*> st1, st2;
            Stack<Node> st1 = new Stack<>();
            Stack<Node> st2 = new Stack<>();
            Node top1, top2;
    
            int count = 0;
            boolean result = false;
          
            // the loop will break when either of two
            // traversals gets completed
            while (true) {
          
                // to find next node in inorder
                // traversal of BST 1
                while (root1 != null) {
                    st1.push(root1);
                    root1 = root1.left;
                }
          
                // to find next node in reverse
                // inorder traversal of BST 2
                while (root2 != null) {
                    st2.push(root2);
                    root2 = root2.right;
                }
          
                // if either gets empty then corresponding
                // tree traversal is completed
                if (st1.empty() || st2.empty())
                    break;
          
                top1 = st1.peek();
                top2 = st2.peek();
          
                // if the sum of the node's is equal to 'target'
                if ((top1.data + top2.data) == target) {
                    // increment count
                    count++;
                    //count = true;
                    //return true;
          
                    // pop nodes from the respective stacks
                    st1.pop();
                    st2.pop();
          
                    // insert next possible node in the
                    // respective stacks
                    root1 = top1.right;
                    root2 = top2.left;
                }
          
                // move to next possible node in the
                // inorder traversal of BST 1
                else if ((top1.data + top2.data) < target) {
                    st1.pop();
                    root1 = top1.right;
                }
          
                // move to next possible node in the
                // reverse inorder traversal of BST 2
                else {
                    st2.pop();
                    root2 = top2.left;
                }
            }
    
            if (count >= 1)
                result = true;
            
            result = false;
            
            // required count of pairs
            //return count;
            //return false;
            return result;
    }
    
    
    /*
    static boolean pairsum(Node root1, Node root2, int target){
    //Enter your code here
        Stack<Node> st1 = new Stack(), st2 = new Stack();
     
        // Initializing forward iterator
        Node c = root1;
        while (c != null)
        {
            st1.push(c);
            c = c.left;
        }
        // Initializing backward iterator
        c = root2;
        while (c != null)
        {
            st2.push(c);
            c = c.right;
        }
     
        // Two pointer technique
        while (st1.size() > 0 && st2.size() > 0)
        {
     
            // To store the value of the nodes
            // current iterators are pointing to
            int top1 = st1.peek().data, top2 = st2.peek().data;
     
            // If found a valid pair
            if (top1 + top2 == target)
                return true;
     
            // Moving forward iterator
            if (top1 + top2 < target)
            {
                c = st1.peek().right;
                st1.pop();
                while (c != null)
                {
                    st1.push(c); c = c.left;
                }
            }
     
            // Moving backward iterator
            else
            {
                c = st2.peek().left;
                st2.pop();
                while (c != null)
                {
                    st2.push(c); c = c.right;
                }
            }
        }
     
        // If no such pair found
        return false;
    }*/