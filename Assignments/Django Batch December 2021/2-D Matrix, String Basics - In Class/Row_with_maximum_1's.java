/*
Problem Statement
Given a boolean matrix of size N*M in which each row is sorted your task is to print the index of the row containing maximum 1's. If multiple answer exist print the smallest one.
Input
First line contains two space separated integers denoting values of N and M. Next N lines contains M space separated integers depicting the values of the matrix. 

Constraints:- 
1 < = M, N < = 1000 
0 < = Matrix[][] < = 1
Output
Print the smallest index (0 indexing) of a row containing the maximum number of 1's.
Example
Sample Input:- 
3 5 
0 1 1 1 1 
0 0 0 1 1 
0 0 0 1 1 

Sample Output:- 
0 

Sample Input:- 
4 4 
0 1 1 1 
1 1 1 1 
0 0 1 1 
1 1 1 1 

Sample Output:- 
1
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
                      Scanner input = new Scanner(System.in);
                      int rows = input.nextInt();
                      int columns = input.nextInt();
                      int[] sumArray = new int[rows];
                      int[][] matrix = new int[rows][columns];
                      for(int i = 0; i < rows; i++){
                          for(int j = 0; j < columns; j++){
                              matrix[i][j] = input.nextInt();
                          }
                      }
                      for(int i = 0; i < rows; i++){
                          int count = 0;
                          for(int j = 0; j < columns; j++){
                              if(matrix[i][j] == 1) count++;
                          }
                          sumArray[i] = count;
                      }
                      int max = sumArray[0];
                      for(int i = 1; i < rows; i++){
                          max = Math.max(max, sumArray[i]);
                      }
                      for(int i = 0; i < rows; i++){
                          if(sumArray[i] == max){
                              System.out.println(i);
                              break;
                          }
                      }
	}
}