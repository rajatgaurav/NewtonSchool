/*
Problem Statement
Given N points. You can get 1000 tickets for 500 points or 5 tickets for 5 points. What is the maximum number of tickets you can earn.
Input
Input contains a single integer N. 

Constraints: 
1 <= N <= 10^15
Output
Print the maximum number of tickets you can earn using atmax N points.
Example
Sample Input 
506 

Sample Output 
1005 

Explanation: we use 500 points to get 1000 tickets and 5 out of remaining 6 points to get 5 tickets making total of 1005 tickets. 

Sample Input:- 
4 

Sample Output:- 
0
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework
// don't change the name of this class
// you can add inner classes if needed
class Main {
    public static void main (String[] args) {
                        // Your code here
        Scanner input = new Scanner(System.in);
        long n = input.nextLong();
        long ans = 0; 
        if(n >= 500){
            ans += (n / 500) * 1000;
            n = n - (500 * (n / 500) );
        }
        if(n >= 5){
            ans += n / 5 * 5;
        }
        System.out.println(ans);            
    }
}