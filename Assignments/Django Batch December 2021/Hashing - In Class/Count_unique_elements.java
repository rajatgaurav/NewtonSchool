/*
Problem Statement
Given an array arr of numbers, your task is to count the number of unique elements in it.
Input
First line of the input contains n (Number of elements). Next line contains the input of the array.

For python users you just have to complete the function. 

Constraints 
1<= n <= 1e6 
1<= arr[i] <= 1e9
Output
Print the number of unique elements in the array 

For python users return the count of unique elements from the given function
Example
Sample Input 1 
4 
1 2 3 3 

Sample Output 1 
3 

Sample Input 2 
6 
1 1 2 2 3 3 

Sample Output 2 
3
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	/*public static void main (String[] args) throws Exception {
                      // Your code here
		//Scanner input = new Scanner(System.in);
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(br.readLine()); 
		//int n = input.nextInt();
		int[] arr = new int[n];
		String line = br.readLine();
		//String[] strs = line.trim().split("\\s+");
		String[] strs = line.trim().split(" ");
		for(int i = 0; i < n; i++){
			//arr[i] = input.nextInt();
			//arr[i] = Integer.parseInt(br.readLine());
			arr[i] = Integer.parseInt(strs[i]);
		}
		HashMap<Integer, String> map = new HashMap<>();
		for(int i = 0; i < n - 1; i++){
			map.put(arr[i], "");
		}
		System.out.print(map.size());
	}
}*/


	private static int countUniqueElements(int[] arr, int n) {
    	HashSet<Integer> map = new HashSet<>();
    	
		for (int i = 0; i < n; i++) {
      		map.add(arr[i]);
    	}
    	int ans = map.size();
    	return ans;
  	}

	/*private static int countUniqueElements(int arr[],int n) {
 
        HashSet<Integer> map = new HashSet<Integer>();
 
        for(int i = 0; i < n; i++) {
            map.add(arr[i]);
        }
       
        return map.size();    
    }*/

	/*static int countUniqueElements(int arr[], int n) {
        // First sort the array so that all
        // occurrences become consecutive
        Arrays.sort(arr);
 
        // Traverse the sorted array
        int res = 0;
        for (int i = 0; i < n; i++) {
 
            // Move the index ahead while
            // there are duplicates
            while (i < n - 1 && arr[i] == arr[i + 1]) {
                i++;
            }
            res++;
        }
        return res;
    }*/

	public static void main (String[] args) throws Exception {
                      // Your code here
		//Scanner input = new Scanner(System.in);
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(br.readLine()); 
		//int n = input.nextInt();
		int[] arr = new int[n];
		String line = br.readLine();
		//String[] strs = line.trim().split("\\s+");
		String[] strs = line.trim().split(" ");
		for(int i = 0; i < n; i++){
			//arr[i] = input.nextInt();
			arr[i] = Integer.parseInt(strs[i]);
		}
		System.out.print(countUniqueElements(arr, n));
	}
}