/*
Problem Statement
Alice wants to go to Bob's house. The location of their houses is given on a 2-D coordinate system. There are a total of 8 directions: 
North - Directly above 
South - Directly below 
East - To the right 
West - To the left 
North East - In between north and east 
North West - In between north and west 
South East - In between south and east 
South West - In between south and west 
Find the direction in which Alice must go to reach Bob's house.
Input
There are two lines of input. The first line contains the x and y coordinate of Alice's house. The second line contains x and y coordinate of Bob's house. It is given that these two locations are different. 

-100 <= Coordinates <= 100
Output
Print a single string denoting the direction in which Alice must move to reach Bob's house.
Example
Sample Input 1: 
2 5 
11 25 

Sample Output 1: 
North East 

Sample Input 2: 
23 12 
-85 12 

Sample Output 2: 
West
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String s = br.readLine();
        String [] str = s.split(" ");
		int x1 = Integer.parseInt(str[0]);
		int y1 = Integer.parseInt(str[1]);
		String s1 = br.readLine();
		String [] str1 = s1.split(" ");
		int x2 = Integer.parseInt(str1[0]);
		int y2 = Integer.parseInt(str1[1]);
		if (x1 == x2 && y1 > y2) {
			System.out.print("South");
		} else if (x1 == x2 && y1 < y2) {
			System.out.print("North");
		} else if (x1 > x2 && y1 == y2) {
			System.out.print("West");
		} else if (x1 < x2 && y1 == y2) {
			System.out.print("East");
		} else if (x1 < x2 && y1 > y2) {
			System.out.print("South East");
		} else if (x1 > x2 && y1 > y2) {
			System.out.print("South West");
		} else if(x1 > x2 && y1 < y2) {
			System.out.print("North West");
		} else if(x1 < x2 && y1 < y2) {
			System.out.print("North East");
		}
	}
}