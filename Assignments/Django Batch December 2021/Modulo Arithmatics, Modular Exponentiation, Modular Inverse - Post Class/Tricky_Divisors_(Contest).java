/*
Problem Statement
You are given N positive integers A[1], A[2]..., A[N]. 
For each index i from 1 to N, find the number of divisors of product of all the elements of the array excluding A[i]. 

As this answers can be huge, output them modulo 1000000007. 

(You may like to read about modulo inverse to solve this problem)
Input
The first line of input contains an integer N. 
The next line contains N singly spaced positive integers. 

Constraints 
2 <= N <= 100000 
1 <= A[i] <= 100000
Output
Output N integers, the answer to this problem.
Example
Sample Input 
3 
5 10 15 

Sample Output 
12 6 6 

Explanation: 
For i=1, 10*15=150 has 12 divisors. 
For i=2, 5*15=75 has 6 divisors. 
For i=1, 5*10=50 has 6 divisors. 

Sample Input:- 
5 
1 2 3 4 5 

Sample Output: 
16 12 8 8 8 
*/

