/*
Problem Statement
You are Player-50 and your opponent is Player-218. You will be given N marbles and each will have a number written on it. But, before this, you along with your opponent are asked to decide a game. 
So, you come up with a game that goes as follows: 
Both of you will take turns, with you starting first. On each turn, the player removes any marble from the given marbles. The player who removes a marble loses if the sum of the values of all removed marbles is divisible by 3. But if there are no remaining marbles, then Player 218 wins(even if it was your turn). 
Since none of you wants to die, players play mindfully. 
Given a function that should return true if you win and false if Player 218 wins. But, there is some logical error. Fix the bug…
Input
First-line contains an integer t - the number of test cases. 
Each of the next pair of lines is as follows: 
First-line contains an integer N - number of marbles. 
Second-line has space-separated integers which are the numbers written on marbles. 

Constraints: 
1 <= N <= 100 
0 <= marble’s value <= 100 
Output
For each test case, the output should be 
1 - if you win 
0 - if you lose (i.e. Player-218 wins)
Example
Input: 
1 
5 
2 3 5 1 4 

Output: 
0 

Explanation: 
Player-218 will always win. One possible way for Player-218 to win is shown below: 
Turn 1: You can remove the marble with value 1. Sum of removed marbles = 1. 
Turn 2: Player-218 can remove the marble with value 3. Sum of removed marbles = 1+3 = 4. 
Turn 3: You can remove the marble with value 4. Sum of removed marbles = 1+3+4 = 8. 
Turn 4: Player-218 can remove the marble with value 2. Sum of removed marbles = 1+3+4+2 = 10. 
Turn 5: You can remove the marble with a value of 5. Sum of removed marbles = 1+3+4+2+5 = 15. 
15 is divisible by 3, hence you lose.
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
        Scanner input = new Scanner(System.in);
        int t = input.nextInt();
        while(t > 0){
            int n = input.nextInt();
            int[] arr = new int[n];
            for(int i = 0 ;i < n ;i++){
                arr[i] = input.nextInt();
            }
            if(playTheGame(arr, n)){
                System.out.println(1);
            }else{
                System.out.println(0);
            }
            t--;
        }
	}

    private static boolean playTheGame(int[] arr, int n){
        HashMap<Integer, Integer> map = new HashMap<>();
        map.put(0,0);
        map.put(1,0);
        map.put(2,0);
        for(int i = 0 ;i < n ;i++){
            Integer remainder = arr[i]%3;
            if(map.containsKey(remainder)){
                Integer prevCount = map.get(remainder);
                map.put(remainder, prevCount + 1);
            }else{
                map.put(remainder, 1);
            }
        }
        if(map.get(0) % 2 == 0){
            return map.get(1) > 0 && map.get(2) > 0;
        }else{
            return Math.abs(map.get(1) - map.get(2)) > 2;
        }
    }
}