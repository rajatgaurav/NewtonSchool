/*
Problem Statement
How many ways are there to place a black and a white knight on an N * M chessboard such that they do not attack each other? The knights have to be placed on different squares. A knight can move two squares horizontally and one square vertically (L shaped), or two squares vertically and one square horizontally (L shaped). The knights attack each other if one can reach the other in one move.
Input
The first line contains the number of test cases T. Each of the next T lines contains two integers N and M which is size of matrix. 

Constraints: 
1 <= T <= 100 
1 <= N, M <= 100
Output
For each testcase in a new line, print the required answer, i.e, number of possible ways to place knights.
Example
Sample Input: 
3 
2 2 
2 3 
4 5 

Sample Output: 
12 
26 
312 

Explanation: 
Test Case 1: We can place a black and a white knight in 12 possible ways such that none of them attacks each other.
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	static long blackAndWhite(int n, int m)
	{
		int X_axis[] = { -2, -1, 1, 2};
		int Y_axis[] = { 1, 2, 2, 1 };
	
		long ret = 0;
	
		for(int i = 0; i < m; ++i)
		{
			for(int j = 0; j < n; ++j)
			{
				for(int k = 0; k < 4; ++k)
				{
					int x = i + X_axis[k];
					int y = j + Y_axis[k];
				
					if (x >= 0 && x < m && y >= 0 && y < n)
						++ret;
				}
			}
		}

		long Total = m * n;
		Total = Total * (Total - 1) / 2;
		return 2 * (Total - ret);
	}

	public static void main (String[] args) 
	{
                      // Your code here
		Scanner input = new Scanner(System.in);
		int T = input.nextInt();
		int N = 0;
		int M = 0;
		for(int i = 0; i < T; i++)
		{
			N = input.nextInt();
			M = input.nextInt();

			System.out.println(blackAndWhite(N, M));
		}
	}
}