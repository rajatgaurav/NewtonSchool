/*
Problem Statement
Initially there was a tree with N nodes (and obviously N-1 edges). Some stupid person removed some of edges of the tree. This mischief divided the tree into M small subtrees disconnected with each other. 

Now, investigating agent Mr. Kapatia wants to know the number of edges that are removed from the tree. Can you tell him this fact?
Input
The first and the only line of input contains two integers N and M, the number of nodes in the initial tree and the number of subtrees after some edges were removed. 

Constraints 
1 <= N <= 1000000000 
1 <= M <= N
Output
Output a single integer, the number of edges that were removed from the tree.
Example
Sample Input 
5 2 

Sample Output 
1 

Explanation: We can create any tree of 5 nodes. If one edge of the tree is removed, two smaller subtrees will be created. 

Sample Input 2 
10 1 

Sample Output 
0
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
        Scanner input = new Scanner(System.in);
        long n = input.nextLong();
        long m = input.nextLong();

        System.out.print(m-1);
	}
}