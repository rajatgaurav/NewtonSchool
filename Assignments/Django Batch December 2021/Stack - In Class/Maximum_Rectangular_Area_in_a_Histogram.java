/*
Problem Statement
Black widow is on a mission to get more information about hydra's whereabouts. She comes across a room where laser lines intersect to form containers. A message pops up on the screen in front of her mocking her saying that she won't be able to find the information she needs. But there's a hint too, the container with maximum water has the intel she needs. The water is polluted with a substance that causes it to become invisible so that no one can make out by seeing how much water is in the container. Help Black Widow solve the mystery and get more intel about hydra. 
Find the largest rectangular area that contains most water in a given histogram where the largest rectangle can be made of a number of contiguous bars. For simplicity, assume that all bars have same width and the width is 1 unit.
Input
First line contains an integer 'N' denoting the size of array. The second line contains N space-separated integers A1, A2, ..., AN denoting the elements of the array. The elements of the array represents the height of the bars. 

Constraints: 
1 <= N <= 10^5 
1 <= A[i] <= 10^9
Output
Print a single line denoting the maximum rectangular area possible from the contiguous bars.
Example
Input 
7 
6 2 5 4 5 1 6 

Output 
12 

Input 
4 
6 3 4 2 

Output 
9
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	static int getMaxArea(int arr[], int n) {
		// Create an empty stack. The stack holds indexes of arr[] array
		// The bars stored in stack are always in increasing order of their
		// heights.
		Stack<Integer> s = new Stack<>();
		
		int max_area = 0; // Initialize max area
		int tp; // To store top of stack
		int area_with_top; // To store area with top bar as the smallest bar
	
		// Run through all bars of given histogram
		int i = 0;
		while (i < n) {
			// If this bar is higher than the bar on top stack, push it to stack
			if (s.empty() || arr[s.peek()] <= arr[i])
				s.push(i++);
	
			// If this bar is lower than top of stack, then calculate area of rectangle
			// with stack top as the smallest (or minimum height) bar. 'i' is
			// 'right index' for the top and element before top in stack is 'left index'
			else {
				tp = s.peek(); // store the top index
				s.pop(); // pop the top
	
				// Calculate the area with arr[tp] stack as smallest bar
				area_with_top = arr[tp] * (s.empty() ? i : i - s.peek() - 1);
	
				// update max area, if needed
				if (max_area < area_with_top)
					max_area = area_with_top;
			}
		}
	
		// Now pop the remaining bars from stack and calculate area with every
		// popped bar as the smallest bar
		while (s.empty() == false) {
			tp = s.peek();
			s.pop();
			area_with_top = arr[tp] * (s.empty() ? i : i - s.peek() - 1);
	
			if (max_area < area_with_top)
				max_area = area_with_top;
		}
	
		return max_area;

	}
	
	// Driver program to test above function
	public static void main(String[] args) {
					  // Your code here
		Scanner input = new Scanner(System.in);
		int n = input.nextInt();
		int[] arr = new int[n];
		for (int i = 0; i < n; i++) {
			arr[i] = input.nextInt();
		}
		System.out.println(getMaxArea(arr, arr.length));
	}
}