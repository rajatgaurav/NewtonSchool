/*
Problem Statement
Given an array Arr of size N and an integer K. Todo wants you to find lexicographically minimum subsequence of the array of size K. 
A subsequence is a sequence that can be derived from array by deleting some or no elements without changing the order of the remaining elements. 
A sequence X is lexicographically smaller than Y if in the first position where X and Y differ, subsequence X has a number less than the corresponding number in Y.
Input
First line of input contains two integers N and K. 
Second line of input contains N integers denoting array Arr. 

Constraints: 
1 <= K <= N <= 100000 
1 <= Arr[i] <= 1000000000
Output
Print K integers denoting the lexicographically minimum subsequence of the array of size K.
Example
Sample Input 1 
5 2 
12 2 1 3 4 

Sample Output 1 
1 3 

Sample Input 2 
5 4 
12 2 1 3 4 

Sample Output 2 
2 1 3 4
*/

