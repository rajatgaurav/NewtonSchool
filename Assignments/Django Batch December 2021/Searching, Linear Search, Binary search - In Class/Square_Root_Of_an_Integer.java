/*
Problem Statement
Given an integer N. The task is to find the square root of N. If N is not a perfect square, then return floor(√N). 

Try to solve the problem using Binary Search.
Input
The first line of input contains number of testcases T. For each testcase, the only line contains the number N. 

Constraints: 
1 ≤ T ≤ 10000 
0 ≤ x ≤ 100000000
Output
For each testcase, print square root of given integer.
Example
Sample Input: 
2 
5 
4 

Sample Output: 
2 
2 

Explanation: 
Testcase 1: Since, 5 is not perfect square, so floor of square_root of 5 is 2. 
Testcase 2: Since, 4 is a perfect square, so its square root is 2.
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	static int SqrtRootIntiger(int x) {
        if (x == 0 || x == 1)
            return x;
        int i = 1, result = 1;
         
        while (result <= x) {
            i++;
            result = i * i;
        }
        return i - 1;
    }
 
    public static void main (String[] args) {
                       // Your code here
		Scanner input = new Scanner(System.in);
		int n = input.nextInt();
		while(n > 0){
			int num = input.nextInt();
			System.out.println(SqrtRootIntiger(num));
			n--;
		}
    }
}