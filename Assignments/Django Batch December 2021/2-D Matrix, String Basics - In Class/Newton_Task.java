/*
Problem Statement
Find the length of the shortest string that contains "newton" as the subsequence atleast N times. 
For eg:- "newtonnn" has three "newton" subsequences.
Input
The first and the only line of input contains the number N. 

Constraints 
1 <= N <= 10^12
Output
Output a single integer, the length of the shortest string containing "newton" as a substring at least N times.
Example
Sample Input 
1 

Sample Output 
6 

Explanation: "newton" is the required string. 

Sample Input 
3 

Sample Output 
8 

Explanation: "newtonnn" or "nnewtonn" are both two examples of valid strings.
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
        Scanner input = new Scanner(System.in);
        long n = input.nextLong();
        // newton {1,1,1,1,1,1}  newttoonn {1,1,1,2,2,2}
        int[] arr = {1,1,1,1,1,1};

        long sub = 1;

        int k = 0; // {1,1,1,1,1,1}
        while(sub < n) {
        	sub = (sub / (arr[k])) * (arr[k] + 1);
        	arr[k] += 1;  // {2,1,1,1,1,1}
        	k++;
        	if(k == 6) {
        		k = 0;
        	}
        }

        long ans = 0;
        for(int i = 0; i < arr.length; i++) {
        	ans += arr[i];
        }
        System.out.println(ans);
	}
}