/*
Problem Statement
There is a function F defined as, 
F(x) = GCD(1, x) + GCD(2, x) +. . + GCD(x, x) 
Your task is to find F(x), for the given x.
Input
First line contain integer T denoting number of test cases.
Next t lines contain an integer x. 

Constraints 
1 <= T <= 20000 
1 <= x <= 100000
Output
For each test case print F(n) in separate line
Example
Sample Input 
5 
1 
2 
3 
4 
5 

Sample output 
1 
3 
5 
8 
9
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	static int getCount(int d, int n) {
		int no = n / d;
		int result = no;

		for(int p = 2; p * p <= no; ++p) {
			if (no % p == 0) {

				while (no % p == 0)
					no /= p;
				result -= result / p;
			}
		}

		if (no > 1)
			result -= result / no;

		return result;
	}

	static int sumOfGCDofPairs(int n) {
		int res = 0;

		for(int i = 1; i * i <= n; i++) {
			if (n % i == 0) {
			
				int d1 = i;
				int d2 = n / i;

				res += d1 * getCount(d1, n);

				if (d1 != d2)
					res += d2 * getCount(d2, n);
			}
		}
		return res;
	}

	public static void main (String[] args) {
                      // Your code here
		Scanner input = new Scanner(System.in);
		int i;
		int n = input.nextInt();
		//int[] x = new int[n];
		for(i = 0; i < n; i++) {
			//x[i] = input.nextInt();
			int x = input.nextInt();

			System.out.println(sumOfGCDofPairs(x));
		}
	}
}