/*
Problem Statement
Given two strings Str1 and Str2 your task is to print the concatenation of the given two strings.
Input
User Task: 
Since this will be a functional problem, you don't have to take input. You just have to complete the function StringConcatenation() that takes the string Str1 and Str2 as input. 

Note:- String will contain uppercase and lowercase English letters.
Output
Return the concatenation of both the strings.
Example
Sample Input:- 
Newton 
School 

Sample Output:- 
NewtonSchool 

Sample Input:- 
Women 
InTech 

Sample Output:- 
WomenInTech
*/

static String StringConcatenation(String Str1, String Str2){
    //Enter your code here
    return Str1.concat(Str2);
    }