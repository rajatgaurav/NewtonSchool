/*
Problem Statement
Given an array of N elements, your task is to find the count of repeated elements in sorted order.
Input
The first line of input contains a single integer N, the next line of input contains N space- separated integers depicting the values of the array. 

Constraints:- 
1 <= N <= 100000 
1 <= Arr[i] <= 100000
Output
For each duplicate element in sorted order in a new line, First, print the duplicate element and then print its number of occurence space- separated. 

Note:- It is guaranteed that at least one duplicate element will exist in the given array.
Example
Sample Input:- 
5 
3 2 1 1 2 

Sample Output:- 
1 2 
2 2 

Sample Input:- 
5 
1 1 1 1 5 

Sample Output:- 
1 4
 */

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
		Scanner input = new Scanner(System.in);
		int n = input.nextInt();
		int[] arr = new int[n];
		for(int i = 0; i < n;i++){
			arr[i] = input.nextInt();
		}
		HashMap<Integer, Integer> map = new HashMap<>();
		for(int i = 0; i < n;i++){
            if(map.containsKey(arr[i])){
                Integer prevCount = map.get(arr[i]);
                map.put(arr[i], prevCount + 1);
            }else{
                map.put(arr[i], 1);
            }
		}

        for(Integer number: map.keySet()){
            if(map.get(number) > 1){
                System.out.println(number + " " + map.get(number));
            }
        }
	}
}