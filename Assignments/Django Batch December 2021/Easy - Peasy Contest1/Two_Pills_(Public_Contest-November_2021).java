/*
Problem Statement
You are fighting against a monster. 
The monster has health M while you have initial health Y. 
To defeat him, you need health strictly greater than the monster's health. 

You have access to two types of pills, red and green. Eating a red pill adds R to your health while eating a green pill multiplies your health by G. 

Determine if it is possible to defeat the monster by eating at most one pill of any kind.
Input
Input contains four integers M (0 <= M <= 104), Y (0 <= Y <= 104), R (0 <= R <= 104) and G (1 <= G <= 104).
Output
Print 1 if it is possible to defeat the monster and 0 if it is impossible to defeat it.
Example
Sample Input 1: 
10 2 2 2 

Sample Output 1: 
0 

Sample Input 2: 
8 7 2 1 

Sample Output 2: 
1 

Explanation for Sample 2: 
You can eat a red pill to make your health : 7 + 2 = 9 > 8.
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework
// don't change the name of this class
// you can add inner classes if needed
class Main {
    public static void main (String[] args) {
                      // Your code here
            Scanner input = new Scanner(System.in);
            int m = input.nextInt();
            int y = input.nextInt();
            int r = input.nextInt();
            int g = input.nextInt();
            if(m < y + r || m < y * g){
                System.out.println("1");
            }else System.out.println("0");
    }
}