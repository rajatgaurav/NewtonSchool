/*
Problem Statement
Tom loves Chess boards. He admires its symmetry and how the black and white colours are placed adjacently along both the axis. 
On his birthday, Tom has been gifted a board which is in the form an NxN grid. Every cell is painted either black or white. Since Tom loves the placement of colours in a chessboard, he will try to convert the board that has been gifted to him identical to a chessboard. He has both black and white colours available to him. Help him find out the minimum number of the cell he will have to recolour to get a chessboard like board.
Input
First line contains an integer N, denoting the size of the board 
Next N lines contains N space separated integers and each integer being either '0' or '1' 
'1' represent black cell 
'0' represents white cell 

Constraints 
1 <= N <= 1000
Output
A single integer that is the minimum number of cells that Tom will have to colour to convert the given board to a chess board
Example
Input: 
3 
1 1 1 
1 1 1 
1 1 1 

Output: 
4 

Explanation: 
Convert to 
1 0 1 
0 1 0 
1 0 1 
Thus we will have to colour 4 cells. 

Input: 
3 
0 1 0 
1 0 1 
0 1 0 

Output: 
0
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		int n=Integer.parseInt(reader . readLine());
		int [][] c=new int [n][n];
		int [][] chess1=new int[n][n];
		int [][] chess2=new int[n][n];
		int count1=0;
		int count2=0;
		
		for(int i=0;i<n;i++){
			String inputs[] = reader . readLine().split(" ");
			for(int j=0;j<n;j++){
				c[i][j]=Integer.parseInt(inputs[j]);
				if((i%2 == 0) == (j%2 == 0)){
						chess1[i][j]=0;
				}
				else{
					chess1[i][j]=1;
				}
				if((i%2 == 0) == (j%2 == 0)){
						chess2[i][j]=1;
				}
				else{
					chess2[i][j]=0;
				}
				count1+=Math.abs(c[i][j]-chess1[i][j]);
				count2+=Math.abs(c[i][j]-chess2[i][j]);
			}
		}
	
		if(count1 > count2){
			System.out.print(count2);
		}
		else
		System.out.print(count1);
	}
}