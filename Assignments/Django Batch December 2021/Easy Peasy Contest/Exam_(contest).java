/*
Problem Statement
Your friend and you took a true/false exam of n questions. You know your answers, your friend’s 
answers, and that your friend got k questions correct. 
Compute the maximum number of questions you could have gotten correctly.
Input
The first line of input contains a single integer k. 
The second line contains a string of n (1 ≤ n ≤ 1000) characters, the answers you wrote down. 
Each letter is either a ‘T’ or an ‘F’. 
The third line contains a string of n characters, the answers your friend wrote down. Each letter 
is either a ‘T’ or an ‘F’. 
The input will satisfy 0 ≤ k ≤ n.
Output
Print, on one line, the maximum number of questions you could have gotten correctly.
Example
Sample Input 
3 
FTFFF 
TFTTT 

Sample Output 
2
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        String a = input.next();
        String b = input.next();
        int x = a.length();
        int count =0;
        for(int i = 0; i < x; i++){
            if(a.charAt(i) == b.charAt(i)){
                count++;
            }
        }
        System.out.print(x - Math.abs(count - n));
	}
}