/*
Problem Statement
Given a linked list consisting of N nodes, your task is to check if the given list is palindrome or not. 

A palindrome is a string/number that mirrors itself, for example, 21312 reverse is also 21312.
Input
User task: 
Since this will be a functional problem, you don't have to take input. You just have to complete the functions Ispalindrome() that takes the head of the linked list as parameter. 

Constraints:- 
1<=N<=1000 
1<=Node.data<=1000
Output
Return true if given list is palindrome else return false.
Example
Sample Input 1:- 
5 
1 2 3 2 1 

Sample Output 1:- 
Yes 

Sample Input 2:- 
3 
1 2 2 

Sample Output 2:- 
No
*/

/*
class Node {
    Node next;
    int val;

    Node(int val) {
        this.val = val;
        next = null;
    }
}
*/
public static boolean IsPalindrome(Node head) {
    //enter your code here
        Node head2 = reverseLinkedList(head);
        while(head != null && head2 != null){
            if(head.val != head2.val){
                return false;
            }
            head = head.next;
            head2 = head2.next;
        }
        return true;
    }
    
    public static Node reverseLinkedList(Node head) {
            // return the head of the modified linked list
            Node prev = null;
            Node current = head;
            Node next = null;
            while(current != null){
                next = current.next;
                current.next = prev;
                prev = current;
                current = next;
            }
            return prev;
        }