/*
Problem Statement
Ram gave Sam few queries. Each query consists of a single number N. Sam can perform any of the 2 operations on N in each move: 
1: If he takes 2 integers a and b where N=a*b, then he can change N=max(a, b) 
2: Decrease the value of N by 1. 
Help Sam to print each line containing the minimum number of moves required to reduce the value of N to 0.
Input
The first line contains the integer Q. 
The next Q lines each contain an integer N. 

Constraints 
1 <= Q <= 1000 
0 <= N <= 1000000
Output
Output Q lines. Each line containing the minimum number of moves required to reduce the value of N to 0.
Example
Input 
2 
3 
4 

Output 
3 
3 

Explanation 
For test case 1, We only have one option that gives the minimum number of moves. 
Follow 3- > 2 - > 1- > 0. Hence, 3 moves. 

For the case 2, we can either go 4- > 3- > 2- > 1- > 0 or 4- > 2- > 1- > 0. 
The 2nd option is more optimal. Hence, 3 moves.
*/

#include <bits/stdc++.h> // header file includes every Standard library
using namespace std;

int main() {
    int max = 1000001;
    int nums[max];
    

    for(int i = 0; i < max; ++i) nums[i] = -1;
    nums[0] = 0; nums[1] = 1; nums[2] = 2; nums[3] = 3;
    

    for(int i = 0; i < max; ++i){
        if(nums[i] == -1 || nums[i] > (nums[i - 1] + 1))
            nums[i] = nums[i - 1] + 1;
        for(int j = 1; j <= i && j * i < max; ++j)
            if(nums[j * i] == -1 || (nums[i] + 1) < nums[j * i])
                nums[j * i] = nums[i] + 1;
    }
    

    int q; cin >> q;
    for(int i = 0; i < q; ++i){
        int n; cin >> n;
        cout << nums[n] << endl;
    }
    return 0;
}