/*
Problem Statement
You are given two integer arrays A and B of sizes N and M respectively. You need to modify the elements of A so that B becomes its subarray. Modifying an element means change the element to any other value. 

Find the minimum number of modifications to achieve this.
Input
The first line of the input contains two integers N and M. 
The second line of the input contains N space separated integers, the elements of array A. 
The third line of the input contains M space separated integers, the elements of array B. 

Constraints 
1 <= M <= N <= 500 
1 <= A[i], B[i] <= 10
Output
Output a single integer, the minimum number of modifications in A to make B its subarray.
Example
Sample Input 
6 3 
3 1 2 1 3 3 
1 2 3 

Sample Output 
1 

Explanation: If you modify A[4] from 1 to 3. A[2]. A[4] represents the array B, so B is its subarray. 

Sample Input 
10 5 
3 4 5 3 4 3 1 3 5 2 
1 4 4 4 3 

Sample Output 
3
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
		Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int m = input.nextInt();
        int N[] = new int[n];
        int M[] = new int[m];
        for(int i = 0; i < n ; i++){
            N[i] = input.nextInt();
        }
        for(int i = 0; i < m; i++){
            M[i] = input.nextInt();
        }
        int MaxCount = 0;
        for(int i = 0; i < n - m + 1; i++){
            int count = 0;
            for(int j = 0, k = i; j < m; j++, k++){
                if(N[k] == M[j]){
                    count++;
                }
            }
            MaxCount = Math.max(MaxCount,count);
        }
        System.out.println(m -MaxCount);
	}
}