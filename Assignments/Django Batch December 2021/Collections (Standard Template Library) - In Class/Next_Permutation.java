/*
Problem Statement
Loki, in his bid to prove his intelligence, comes up with a new problem. He gives the Avengers an array containing n elements and asks them to implement the next permutation which rearranges numbers into the lexicographically next greater permutation of numbers. If such an arrangement is not possible, it must rearrange itself as the lowest possible order (i. e., sort in ascending order). The replacement must be in place and use only constant extra memory.
Input
The first line of input contains n, the total elements of the array. 
The next line contains n space- separated integers denoting the array elements. 

Constraints: 
1 <= n <= 10 
0 <= a[i] < 100000
Output
The output contains n space- separated integers denoting the array elements after all the operations.
Example
Sample Input:- 
3 
1 2 3 

Sample Output: 
1 3 2 

Sample Input: 
3 
3 2 1 

Sample Output: 
1 2 3
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[] arr = new int[n];
            
        for(int i = 0; i < n; i++) arr[i] = input.nextInt();
            nextPermutation(arr, n);
            
        for(int i = 0; i < n; i++) {
            System.out.print(arr[i] + " ");
        }
	}

    private static void nextPermutation(int[] arr, int n) {
        for(int i = n - 1; i > 0; i--){
            for(int j = i - 1; j >= 0; j--) {
                if(arr[i] > arr[j]){
                    swap(arr, i, j);
                    Arrays.sort(arr, j + 1, n);
                    return;
                }
            }
        }
        Arrays.sort(arr);
    }

    private static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}