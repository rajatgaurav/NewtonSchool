/*
Problem Statement
Given an integer array A[] of size N, the task is to find the maximum of the minimum of every window size in the array. 
In simple terms, for a particular window type you need to consider all windows of this particular size in the array, take the maximum element of each window, then take the minimum of these maximums. 

Note: Window size varies from 1 to n.
Input
The first line contains an integer N denoting the size of the array. The second line contains N space-separated integers A1, A2, ..., AN denoting the elements of the array. 

Constraint:- 
1 <= N <= 10^5 
1 <= A[i] <= 10^6
Output
Print the array of numbers of size N for each of the considered window size 1, 2, ..., N respectively.
Example
Sample Input: 
7 
10 20 30 50 10 70 30 

Sample Output: 
70 30 20 10 10 10 10 

Explanation: 
Testcase 1: 
First element in output indicates maximum of minimums of all windows of size 1. Minimums of windows of size 1 are {10}, {20}, {30}, {50}, {10}, {70} and {30}. Maximum of these minimums is 70. 
Second element in output indicates maximum of minimums of all windows of size 2. Minimums of windows of size 2 are {10}, {20}, {30}, {10}, {10}, and {30}. Maximum of these minimums is 30. 
Third element in output indicates maximum of minimums of all windows of size 3. Minimums of windows of size 3 are {10}, {20}, {10}, {10} and {10}. Maximum of these minimums is 20. 
Similarly other elements of output are computed.
*/

#include <bits/stdc++.h>
using namespace std;
vector <int> maxOfMin(int array[], int n)
    {

    int size = n;
    vector<int> res(size, 0);
    stack<int> s;
    for (int i = 0; i < size; i++)
    {
        while ((!s.empty()) && (array[s.top()] >= array[i]))
        {
            int top = s.top();
            s.pop();
            int temp = (s.empty() ? i : i - s.top() - 1);
            res[temp - 1] = max(res[temp - 1], array[top]);
        }
        s.push(i);
    }
    while (!s.empty())
    {
        int top = s.top();
        s.pop();
        int temp = (s.empty() ? size : size - s.top() - 1);
        res[temp - 1] = max(res[temp - 1], array[top]);
    }
    for (int i = size - 2; i >= 0; i--)
    {
        if(res[i] < res[i+1]){
            res[i] = res[i+1];
        }
    }
    return res;
    }
int main() {

        int n;
        cin >> n;
        int a[n];
        for (int i = 0; i < n; i++) cin >> a[i];
        vector <int> res = maxOfMin(a, n);
        for (int i : res) cout << i << " ";
        cout << endl;
    return 0;
}