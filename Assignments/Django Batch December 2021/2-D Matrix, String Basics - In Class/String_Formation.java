/*
Problem Statement
Given three strings A, B and C check if string A and B can be combined to form the string C in such a way that the relative order of characters in string A and that of string B does not change.
Input
Input contains a single line contains three strings A, B and C separated by spaces. 

Constraints:- 
1 < = |A|, |B|, |C| < = 5000 

Note:- 
String will contain only lowercase english letters.
Output
Print 1 if the string can be formed else print 0.
Example
Sample Input:- 
aabcc dbbca aadbbcbcac 

Sample Output:- 
1 

Sample Input:- 
ac bd dbac 

Sample Output:- 
0
*/

import java.io.*;
import java.util.*;

public class Main {
	public static void main (String[] args) {
                      // Your code here
    	Scanner input = new Scanner(System.in);
    	String A = input.next();
    	String B = input.next();
    	String C = input.next();
    	if(solution2(A, B, C))
			System.out.println(1);
		else
			System.out.println(0);
	}

  	public static boolean solution2(String A, String B, String C) {
    	if (A.length() + B.length() != C.length())
      		return false;

    	boolean[][] dp = new boolean[A.length() + 1][B.length() + 1];

    	for (int i = 0 ; i < dp.length; i++) {
      		for (int j = 0 ; j < dp[0].length; j++) {
        		if (i == 0 && j == 0) {
          			dp[i][j] = true;
        	} else if (i == 0) {
          		dp[i][j] = B.charAt(j - 1) == C.charAt(i + j - 1) ? dp[i][j - 1] : false;
        	} else if (j == 0) {
          		dp[i][j] = A.charAt(i - 1) == C.charAt(i + j - 1) ? dp[i - 1][j] : false;
        	} else {
          		if (A.charAt(i - 1) == C.charAt(i + j - 1)) {
            		dp[i][j] = dp[i - 1][j];
          	}
          	if (!dp[i][j] && B.charAt(j - 1) == C.charAt(i + j - 1)) {
            	dp[i][j] = dp[i][j - 1];
          		}
        	}
      	}
    }
    return dp[dp.length - 1][dp[0].length - 1];
}
  	public static boolean solution(String A, String B, String C, int i, int j) {
    	if (i == A.length() && j == B.length()) {
      		return true;
    	}
    	if (i < A.length() && A.charAt(i) == C.charAt(i + j)) {
      		if (solution(A, B, C, i + 1, j)) {
        		return true;
      		}
    	}
    	if (j < B.length() && B.charAt(j) == C.charAt(i + j)) {
      		if (solution(A, B, C, i, j + 1)) {
        		return true;
      		}
    	}
    	return false;
  	}
}