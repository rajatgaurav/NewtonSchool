/*
Problem Statement
Ishaan is playing with strings these days. He has found a new string. He wants to modify it as per the following rules: 

1. The string should not have three consecutive same characters (Refer example for explanation). 
2. He can add any number of characters anywhere in the string. 
Find the minimum number of characters which Ishaan must insert in the string.
Input
First line of input contains a single integer T denoting the number of test cases. The only line of each test case contains a string S consisting of lowercase English Alphabets. 

1 <= T <= 100 
1 <= Length of S <= 1000
Output
For each test case, in a new line, print the minimum number of characters which Ishaan must insert in the string.
Example
Sample Input: 
3 
aabbbcc 
aaaaa 
abcddee 

Sample Output: 
1 
2 
0 

Explanation : 
Testcase 1: 
aabbbcc 
3 b's occur consecutively, we add a 'd', 
aabbdbcc 

Testcase 2: 
aaaaa 
5 a's occur consecutively, we need to add 2 'b', 
aababaa 

Testcase 3 : 
abcddee 
No character occurs 3 times, so no need to add anything.
*/

//C++ code

#include <bits/stdc++.h> // header file includes every Standard library
using namespace std;
int main() {
    // Your code here
    int t;
    cin>>t;
    while(t--){
        string s;
        cin>>s;
        int count=1;
        int m=0;
        for(int i=0;i<s.length()-1;i++){
            if(s[i]==s[i+1])
            count++;
            else{
                if(count>=3){
                    if(count%2!=0)
                        m+=count/2;
                        else
                        m+=count/2-1;
                }
                    count=1;
            }
        }
        if(count>=3){
            if(count%2!=0){
                m+=count/2;
            }else{
                m+=count/2-1;
            }
        }
        cout<<m<<endl;
    }
    return 0;
}