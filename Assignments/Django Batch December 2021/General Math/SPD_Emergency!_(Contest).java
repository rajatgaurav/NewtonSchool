/*
Problem Statement
Our five power rangers have powers P1, P2, P3, P4, and P5. 
To ensure the proper distribution of power, the power of every power ranger must remain less than the sum of powers of other power rangers. 
If the above condition is not met, there's an emergency. Can you let us know if there's an emergency?
Input
The first and the only line of input contains 5 integers P1, P2, P3, P4, and P5. 


Constraints 
0 <= P1, P2, P3, P4, P5 <= 100
Output
Output "SPD Emergency" (without quotes) if there's an emergency, else output "Stable".
Example
Sample Input 
1 2 3 4 5 

Sample Output 
Stable 

Explanation 
The power of every power ranger is less than the sum of powers of other power rangers. 

Sample Input 
1 2 3 4 100 

Sample Output 
SPD Emergency 

Explanation 
The power of the 5th power ranger (100) is not less than the sum of powers of other power rangers (1+2+3+4=10).
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
        // Your code here
		Scanner input = new Scanner(System.in);
		int p1 = input.nextInt();
		int p2 = input.nextInt();
		int p3 = input.nextInt();
		int p4 = input.nextInt();
		int p5 = input.nextInt();

		if ( p1+p2+p3+p4 > p5 && p2+p3+p4+p5 > p1 && p1+p3+p4+p5 > p2 && p1+p2+p4+p5 > p3 && p1+p2+p3+p5 > p4) {
			System.out.println("Stable");
		} else {
			System.out.println("SPD Emergency");
		}
	}
}