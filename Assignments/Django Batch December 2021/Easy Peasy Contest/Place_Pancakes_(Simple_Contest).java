/*
Problem Statement
You have a tray X centimetres long. You need to place pancakes having length Y on this tray. Two pancakes must have a distance of atleast Z between them to avoid mixing with each other when the tray is moved. The distance between the end of the tray and the pancake must also be atleast Z so that the pancake stays stable. 
Find the maximum number of pancakes that can be placed on the tray.
Input
The first and the only line of input contains three integers X, Y, and Z. 

Constraints 
1 <= X, Y, Z <= 100000 
You can place atleast one pancake on the tray, i. e, X >= Y + 2Z.
Output
Output a single integer, the maximum number of pancakes you can place on the tray.
Example
Sample Input 
14 3 1 

Sample Output 
3 

Explanation 
This is the arrangement (1 space) Pancake (2 space) Pancake (1 space) Pancake (1 space). 

Sample Input 
12 3 1 

Sample Output 
2
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
        Scanner input = new Scanner(System.in);

        int x = input.nextInt();
        int y = input.nextInt();
        int z = input.nextInt();
        int count = 0;
        
        for(int i = 0; i < x / y; i++){
            if(x >= (y + 2 * z) + i * (y + z)){
                count++;
            }
        }
        System.out.println(count);
	}
}