/*
Problem Statement
A magic number is a natural number that contains both the digits 7 and 9 in it. For eg:- 79, 879, 53729 etc. 
Given a number N, your task is to find the closest Magic number from the given number N. 

Note:- If more than one answer exists return the minimum one
Input
User Task: 
Since this will be a functional problem, you don't have to take input. You just have to complete the function MagicNumber() that takes integer N as argument. 

Constraints:- 
1 <= N <= 100000
Output
Return a magic number closest to the given number N.
Example
Sample Input:- 
8 

Sample Output:- 
79 

Sample Input:- 
900 

Sample Output:- 
897 
*/

static int MagicNumber(int N)
{
    int z=0;
    if(N>=1 && N<=100000)
    {
        if(N<=78)
        {
            z=79;
        }
        else
        {
            boolean a=false,b=false;
            int g=0,f=0;
            for( int i=N;i>0;i--)
            {
                int c=i,d=i;
                while(c!=0)
                {
                    f=c%10;
                    if(f==7)
                    {
                        a=true;
                    }
                    if(f==9)
                    {
                        b=true;
                    }
                    c=c/10;
                    if(a==true && b==true)
                    {
                        g=d;
                        break;
                    }
                }
                a=false;
                b=false;
                if(g!=0)
                {
                    break;
                }
            }
            boolean p=false,q=false;
            int r=0,s=0;
            for( int j=N+1;j<=100000;j++)
            {
                int t=j,u=j;
                while(t!=0)
                {
                    s=t%10;
                    if(s==7)
                    {
                        p=true;
                    }
                    if(s==9)
                    {
                        q=true;
                    }
                    t=t/10;
                    if(p==true && q==true)
                    {
                        r=u;
                        break;
                    }
                }
                p=false;
                q=false;
                if(r!=0)
                {
                    break;
                }
            }
            int x=N-g,y=r-N;
            if(r==0)
            {
                z=g;
            }
            else
            {
                if(x>y)
                {
                    z=r;
                }
                else
                {
                    z=g;
                }
            }
        }
    
    }
    return z;
}