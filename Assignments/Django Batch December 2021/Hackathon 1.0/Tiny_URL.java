/*
Problem Statement
Design a system that takes big URLs like “http://www. geeksforgeeks.org/count-sum-of-digits-in-numbers-from1-to-n/” and converts them into a short 6 character URL. It is given that URLs are stored in database and every URL has an associated integer id. So your program should take an integer id and generate a 6 character long URL. 

A URL character can be one of the following 

A lower case alphabet [‘a’ to ‘z’], total 26 characters 
An upper case alphabet [‘A’ to ‘Z’], total 26 characters 
A digit [‘0′ to ‘9’], total 10 characters 
There are total 26 + 26 + 10 = 62 possible characters. 

So the task is to convert an integer (database id) to a base 62 number where digits of 62 base are "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
Input
The first line contains T denoting the number of test cases. 
The next T lines contain a single integer N. 

1 <= T <= 100000 
1 <= N <= 1000000000
Output
For each test case, in a new line, print the shortened string.
Example
Sample Input: 
1 
12345 

Sample Output: 
dnh 

Explanation: 
Try to convert 12345 in base-62, replace the integer with the corresponding character
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static StringBuffer convert_to_62(int n){
		StringBuffer str = new StringBuffer("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
		StringBuffer ans = new StringBuffer("");  
		while (n > 0){
			ans.append(str.charAt(n%62));
			n = n/62;
		}
		return ans.reverse();

	}
	public static void main (String[] args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int t = Integer.parseInt(br.readLine());
		while (t > 0){
			int n = Integer.parseInt(br.readLine());
			System.out.println(convert_to_62(n));
			t--;
		}
	}
}