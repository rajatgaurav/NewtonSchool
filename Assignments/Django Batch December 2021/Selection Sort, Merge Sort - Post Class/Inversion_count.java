/*
Problem Statement
Calculate inversion count of array of integers. 
Inversion count of an array is quantisation of how much unsorted an array is. A sorted array has inversion count 0, while an unsorted array has maximum inversion count. 
Formally speaking inversion count = number of pairs i, j such that i < j and a[i] > a[j].
Input
The first line contain integers N. 
The second line of the input contains N singly spaces integers. 

1 <= N <= 100000 
1 <= A[i] <= 1000000000
Output
Output one integer the inversion count.
Example
Sample Input 
5 
1 1 3 2 2 

Sample Output 
2 

Sample Input 
5 
5 4 3 2 1 

Sample Output 
10
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	private static long merge(long arr[], int left, int mid, int right) {
        int i = left, j = mid, k = 0;
        long invCount = 0;
        long temp[] = new long[(right - left + 1)];
 
        while ((i < mid) && (j <= right)) {
            if (arr[i] <= arr[j]) {
                temp[k] = arr[i];
                ++k;
                ++i;
            } 
            else {
                temp[k] = arr[j];
                invCount += (mid - i);
                ++k;
                ++j;
            }
        }
 
        while (i < mid) {
            temp[k] = arr[i];
            ++k;
            ++i;
        }
 
        while (j <= right) {
            temp[k] = arr[j];
            ++k;
            ++j;
        }
 
        for (i = left, k = 0; i <= right; i++, k++) {
            arr[i] = temp[k];
        }
 
        return invCount;
    }
    private static long mergeSort(long arr[], int left, int right) {
        long invCount = 0;
 
        if (right > left) {
            int mid = (right + left) / 2;
 
            invCount = mergeSort(arr, left, mid);
            invCount += mergeSort(arr, mid + 1, right);
            invCount += merge(arr, left, mid + 1, right);
        }
        return invCount;
    }
 
    public static long getInversions(long arr[], int n) {
        return mergeSort(arr, 0, n - 1);
    }

	public static void main (String[] args) {
                      // Your code here
		Scanner input = new Scanner(System.in);
		int n = input.nextInt();
        long[] arr = new long[n];

        for(int i = 0; i < n; i++){
            arr[i] = input.nextInt();
        }
		System.out.print(getInversions(arr, n) + " ");
	}
}