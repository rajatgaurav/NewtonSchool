/*
Problem Statement
You have a house, which you model as an axis-aligned rectangle with corners at (x1, y1) and (x2, y2). 
You also have a goat, which you want to tie to a fence post located at (x, y), with a rope of length 
l. The goat can reach anywhere within a distance l from the fence post. 
Find the largest value of l so that the goat cannot reach your house.
Input
Input consists of a single line with six space-separated integers x, y, x1, y1, x2, and y2. All the 
values are guaranteed to be between −1000 and 1000 (inclusive). 
It is guaranteed that x1 < x2 and y1 < y2, and that (x, y) lies strictly outside the axis-aligned 
rectangle with corners at (x1, y1) and (x2, y2).
Output
Print, on one line, the maximum value of l, rounded and displayed to exactly three decimal places
Example
sample input 
7 4 0 0 5 4 

sample output 
2.000 

sample input 
6 0 0 2 7 6 

sample output 
2.000 

sample input 
4 8 7 8 9 9 

sample output 
3.000
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
    public static void main (String[] args) {
                        // Your code here
        Scanner input = new Scanner(System.in);
        
        int x = input.nextInt();
        int y = input.nextInt();
        int x1 = input.nextInt();
        int y1 = input.nextInt();
        int x2 = input.nextInt();
        int y2 = input.nextInt();
        
        double p = Math.min(Math.abs(y - y2),Math.abs(y1 - y));
        double q = Math.min(Math.abs(x - x2),Math.abs(x1 - x));
        
        if (x1 <= x && x <= x2){
            String s = String.format("%.3f", p);
            System.out.print(s);
        }
        else {
            if (y1 <= y && y <= y2) {
                String s = String.format("%.3f", q);
                System.out.print(s);
            }
            else {
                double r = Math.sqrt((p * p) + (q * q));
                String s = String.format("%.3f", r);
                System.out.print(s);
            }
        }
    }
}