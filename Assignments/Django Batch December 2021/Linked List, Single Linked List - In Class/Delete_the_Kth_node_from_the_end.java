/*
Problem Statement
Given a linked list consisting of N nodes and an integer K, your task is to delete the Kth node from the end of the linked list
Input
User Task: 
Since this will be a functional problem, you don't have to take input. You just have to complete the function deleteElement() that takes head node and K as parameter. 

Constraints: 
1 <=K<=N<= 1000 
1 <=Node.data<= 1000
Output
Return the head of the modified linked list
Example
Input 1: 
5 3 
1 2 3 4 5 

Output 1: 
1 2 4 5 

Explanation: 
After deleting 3rd node from the end of the linked list, 3 will be deleted and the list will be as 1, 2, 4, 5. 

Input 2:- 
5 5 
8 1 8 3 6 

Output 2:- 
1 8 3 6
*/

/*
class Node {
    Node next;
    int val;

    Node(int val) {
        this.val = val;
        next = null;
    }
}
*/


public static Node deleteElement(Node head, int k) {
    
    Node start = head;
    Node end = head;
    while(k-- > 0){
        end = end.next;
    }
    if(end == null){
        start = start.next;
        return start;
    }

    while(end.next != null){
        start = start.next;
        end = end.next;
    }

    if(start.next.next == null){
        start.next = null;
        return head;
    }

    start.next = start.next.next;
    return head;

}

/*
public static Node deleteElement(Node head,int k) {
//enter your code here
    if(head == null){
        return null;
    }
    Node slow = head;
    Node fast = head;
    int count = 0;
    while(count <= k-1){
        fast = fast.next;
        count++;
    }
    if(fast == null){
        return head.next;
    }
   
    while(fast.next!= null){
        fast = fast.next;
        slow = slow.next;
    }
    slow.next = slow.next.next;
    return head;
}
*/