/*
Problem Statement
A tuple (R, G, B) is considered good if R*G+B <= N and R, G and B are all positive. Given N, find number of good tuples.
Input
Input contains a single integer N. 

Constraints: 
1 <= N <= 1000000
Output
Print number of good tuples.
Example
Sample Input 
3 

Sample Output 
4 

Explanation: 
Following are the good tuples: 
(1, 1, 1) 
(1, 1, 2) 
(1, 2, 1) 
(2, 1, 1)
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        long count = 0;
        for(int i = 1; i <= n; i++){
            for(int j = 1; i * j <= n; j++) {
                count += n - i * j;
            }                    
        }
        System.out.println(count);
	}
}