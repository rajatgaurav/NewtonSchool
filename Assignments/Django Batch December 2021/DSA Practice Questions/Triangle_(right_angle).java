/*
Problem Statement
In this task you need to print a right angle triangle of height 5.
Input
No input
Output
Print the right angle triangle of height 5 as shown.
Example
Sample Output:- 
* 
* * 
* * * 
* * * * 
* * * * *
*/

static void printTriangle(){

    int n=5;
    
    for(int i = 1; i <= n; i++){
        for(int j = 1; j <= i; j++){
            System.out.print("* ");
        }
        System.out.println();
    }
    
    }