/*
Problem Statement
Modulo exponentiation is super awesome. But can you still think of a solution to the following problem? 

Given three integers {a, b, c}, find the value of a**(b**c) % 1000000007. 
Here a**b means a raised to the power b or pow(a, b). Expression evaluates to pow(a, pow(b, c)) % 1000000007. 


(Read Euler's Theorem before solving this problem)
Input
The first input line has an integer t: the number of test cases. 
After this, there are n lines, each containing three integers a, b and c. 


Constraints 
1≤ t ≤ 100 
0 ≤ a, b, c ≤ 1000000000
Output
For each test case, output the value corresponding to the expression.
Example
Sample Input 
3 
3 7 1 
15 2 2 
3 4 5 

Sample Output 
2187 
50625 
763327764
*/

#include <bits/stdc++.h> // header file includes every Standard library
using namespace std;

long long powmod(long long x, long long n, long long m) {
	if (n == 0) return 1;
  	if (n % 2 == 0) return powmod((x * x) % m, n / 2, m);

  	return (x * powmod(x, n - 1, m)) % m;
}

int main() {	
	int n;
	scanf("%d", &n);
	long long res = 0;
	long long a, b, c;
	
	while (n--) {
  		scanf("%lld %lld %lld", &a, &b, &c);
     	res = powmod(b, c, 1000000006LL);
 		res = powmod(a, res, 1000000007LL);
  		printf("%lld\n", res);
	}
	return 0;
}