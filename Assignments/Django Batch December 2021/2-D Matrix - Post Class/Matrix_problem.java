/*
Problem Statement
Given a binary square matrix of size N*N and an integer K, your task is to print the maximum side of sub square matrix containing at most K 1's.
Input
The first line of input contains two integers N and K, Next N lines contain N space-separated integers depicting the values of the matrix. 

Constraints: 
1 < = N < = 500 
1 < = K < = 100000 
0 < = Matrix[][] < = 1
Output
Print the maximum side.
Example
Sample Input:- 
3 2 
1 1 1 
1 0 1 
1 1 0 

Sample Output:- 
2 

Explanation:- 
0 1 
1 0 
is the required sub matrix. 

Sample Input:- 
2 1 
1 0 
0 1 

Sample Output:- 
1
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
    public static int matrixBlocks(int[][] matrix) {
        for(int i = 1; i < matrix.length; i++){
            matrix[i][0] += Math.min(matrix[i - 1][1], matrix[i - 1][2]);
            matrix[i][1] += Math.min(matrix[i - 1][0], matrix[i - 1][2]);
            matrix[i][2] += Math.min(matrix[i - 1][0], matrix[i - 1][1]);
        }
        int arraySize = matrix.length - 1;
        System.out.println(Math.min(Math.min(matrix[arraySize][0], matrix[arraySize][1]), matrix[arraySize][2]));
        return Math.min(Math.min(matrix[arraySize][0], matrix[arraySize][1]), matrix[arraySize][2]);
    }
     
    public static void main (String[] args) {
                      // Your code here
        Scanner input = new Scanner(System.in);
        int arraySize = input.nextInt();
        int[][] matrix = new int[arraySize][arraySize];
        for(int i = 0; i < arraySize; i++){
            for(int j = 0; j < arraySize; j++){
                matrix[i][j] = input.nextInt();
            }
        }
        matrixBlocks(matrix);
    }
}