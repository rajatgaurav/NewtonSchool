/*
Problem Statement
In sheet each row is assigne as a string as shown:- 
A - > 1 
B - > 2 
C - > 3 
. . 
Z - > 26 
AA - > 27 
AB - > 28 

Given the string S, your task is to find its row number.
Input
Input contains a single line containing the string S. 

Constraints:- 
1 < = |S| < = 100000 

Note:- String will only contain uppercase english letters
Output
Print a single integer containing the row number since the ans can be quite large print you ans modulo 10^9 + 7.
Example
Sample Input:- 
AB 

Sample Output:- 
28 

Sample Input:- 
A 

Sample Output:- 
1
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
		Scanner input = new Scanner(System.in);
		String str = input.nextLine();

		System.out.println(columnNo(str));
	
	}
	private static long columnNo(String s){
		long result = 0;
		int n = s.length();
		char[] arr = s.toCharArray();

		for(int i=0; i<n; i++){
			result = (result*26 + arr[i] - 'A' + 1)%1000000007;
		}
		return result;
	}
}