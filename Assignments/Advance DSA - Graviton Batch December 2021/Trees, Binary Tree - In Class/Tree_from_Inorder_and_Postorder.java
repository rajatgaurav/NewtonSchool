/*
Problem Statement
Given inorder and postorder traversals of a Binary Tree. The task is to construct the binary tree from these traversals. It is given that 1 is the root of the tree.
Input
The first line of input contains a single integer N, denoting the number of nodes in the tree. The next line contains N integers denoting the inorder traversal of the tree. The next line contains N integers denoting the postorder traversal of the tree. 

Constraints 
1 <= N <= 100000
Output
Print the preorder traversal of the given tree.
Example
Sample Input 1: 
8 
4 8 2 5 1 6 3 7 
8 4 5 2 6 7 3 1 

Sample Output 1: 
1 2 4 8 5 3 6 7 

Explanation: 
For the given inorder and postorder traversal, the resultant binary tree will be 
1 
/ \ 
2 3 
/ \ / \ 
4 5 6 7 
\ 
8 

Sample Input 2: 
5 
9 5 2 3 4 
5 9 3 4 2 

Sample Output 2: 
2 9 5 4 3 

Explanation: 
For the given inorder and postorder traversal, the resultant binary tree will be 
2 
/ \ 
9 4 
\ / 
5 3
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
        Scanner input = new Scanner(System.in);
        
        int n = input.nextInt();

        int[] inorder = new int[n];
        int[] postorder = new int[n];
        
        Main tree = new Main();
        for (int i = 0; i < n; i++) {
            inorder[i] = input.nextInt();
        }
        for (int i = 0; i < n; i++) {
            postorder[i] = input.nextInt();
        }
        Node root = tree.buildUtil(inorder, postorder, 0, n - 1, 0, n - 1);
		tree.preOrder(root);
	}

    Node buildUtil(int in[], int post[], int inStrt, int inEnd, int postStrt, int postEnd) {
		if (inStrt > inEnd)
			return null;

		Node node = new Node(post[postEnd]);

		if (inStrt == inEnd)
			return node;
		int iIndex = search(in, inStrt, inEnd, node.data);

		node.left = buildUtil(in, post, inStrt, iIndex - 1, postStrt, postStrt - inStrt + iIndex - 1);
		node.right = buildUtil(in, post, iIndex + 1, inEnd, postEnd - inEnd + iIndex, postEnd - 1);

		return node;
	}

	int search(int arr[], int strt, int end, int value) {
		int i;
		for (i = strt; i <= end; i++) {
			if (arr[i] == value)
				break;
		}
		return i;
	}

	void preOrder(Node node) {
		if (node == null)
			return;
		System.out.print(node.data + " ");
		preOrder(node.left);
		preOrder(node.right);
	}

}

class Node {
	int data;
	Node left, right;

	public Node(int data)
	{
		this.data = data;
		left = right = null;
	}
}