/*
Problem Statement
Given a sorted array of N integers a[], and Q queries. For each query, you will be given one element K your task is to print the maximum element from the array which is less than or equal to the given element(Floor), and the minimum element from the array which is greater than or equal to the given element(Ceil).
Input
In case of Java only 
User Task: 
Since this will be a functional problem, you don't have to take input. You just have to complete the function floorAndCeil() that takes the array a[], integer N and integer k as arguments. 

Custom Input 
The first line of input contains a single integer N. The Second line of input contains N space-separated integers depicting the values of the array. The third line of input contains a single integer Q. The next Q line of input contains a single integer the value of K. 

Constraints:- 
1 <= N <= 100000 
1 <= K, Arr[i] <= 1000000000000 
1 <= Q <= 10000
Output
In a new line Print two space-separated integers depicting the values of Floor and Ceil of the given number. If the floor or ceil of the element does not exist print -1.
Example
Sample Input:- 
5 
2 5 6 11 15 
5 
2 
4 
8 
1 
16 

Sample Output:- 
2 2 
2 5 
6 11 
-1 2 
15 -1
*/

static void floorAndCeil(int a[], int N, int x){	
    //Enter your code here
        int floorIndex = getFloor(a, N, x);
        if(floorIndex == 0){
            if(a[floorIndex] == x){
                System.out.println(x + " " + x);
            }else{
                System.out.println("-1 " + a[floorIndex]);
            }
        }else if(floorIndex == N){
            System.out.println(a[floorIndex - 1] + " " + -1);
        }else{
            if(a[floorIndex] == x){
                System.out.println(x + " " + x);
            }else{
                System.out.println(a[floorIndex - 1] + " " + a[floorIndex]);
            }
        }
    }
    
    static int getFloor(int[] arr, int n, int x){
        int low = 0;
        int high = n -1;
        int resultIndex = n;
        while(low <= high){
            int mid = low + (high-low)/2;
            if(arr[mid] < x){
                low = mid + 1;
            }else{
                high = mid - 1;
                resultIndex = mid;
            }
        }
        return resultIndex;
    }