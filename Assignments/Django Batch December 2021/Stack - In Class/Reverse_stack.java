/*
Problem Statement
Given a stack containing some integers, your task is to reverse the given stack. 

Note:- Try to do this question using recursion, do not use any loop.
Input
User task: 
Since this will be a functional problem, you don't have to take input. You just have to complete the functions Reverse_stack() that takes no parameter. 

Constraints: 
1 <= Elements in stack <= 100 

Custom Input: 
First line of input should contain the number of elements N of the stack, the next line of input should contain N space separated integers depicting the elements of the stack. 
Output
You don't need to return or print anything just complete the given function. 

Note:- For the custom input if your code is correct then the elements will be printed in the same order.
Example
Sample Input:- 
Stack = {1, 2, 3, 4, 5}, where top = 5 

Sample Output:- 
Stack = {5, 4, 3, 2, 1} where top = 1
*/

#include <bits/stdc++.h> // header file includes every Standard library
using namespace std;

void insertAtBottom(stack<int> &s, int item)
{
    if (s.empty())
    {
        s.push(item);
        return;
    }

    int top = s.top();
    s.pop();
    insertAtBottom(s, item);

    s.push(top);
}
 
void reverseStack(stack<int> &s)
{

    if (s.empty()) {
        return;
    }

    int item = s.top();
    s.pop();
    reverseStack(s);

    insertAtBottom(s, item);
}
 
int main() {

	// Your code here
    stack<int> s;

	int n;
	cin>>n;
	int arr[n];
	for(int i=0;i<n;i++){
		cin>>arr[i];
	}
    for (int i = 0; i <n; i++) {
        s.push(arr[i]);
    }
 
    reverseStack(s);
 

    while (!s.empty())
    {
        cout << s.top() << ' ';
        s.pop();
    }
 
    return 0;
}


/*


// static Stack <Integer> St = new Stack();
static void Reverse_Stack(){
//Enter your code here
    if(St.isEmpty()){
        return;
    }
    int x = St.pop();
    Reverse_Stack();
    bottomInsert(x);
}

static void bottomInsert(int x){
    if(St.isEmpty()){
        St.push(x);
    }else{
        int i = St.pop();
        bottomInsert(x);
        St.push(i);
    }
}

*/