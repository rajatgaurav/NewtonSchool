/*
Problem Statement
You are given a tree rooted at 1 (root is at level 1). The entire tree can be created in the following fashion:- 
Nodes at odd level have 2 children. So node 1 has two children, nodes 2 and 3. 
Nodes at even level have 4 children. So node 2 has 4 children, nodes 4, 5, 6, and 7. Similarly node 3 has 4 children, nodes 8, 9, 10, and 11. 
This process goes on infinitely. Node 4 has 2 children, nodes 12 and 13, node 5 has 2 children, nodes 14 and 15,... , and so on. 

Now you are given an integer N, you need to report the path from the root to the node.
Input
The first and the only line of input contains an integer N. 

Constraints 
1 <= N <= 1016
Output
Output the path from the root to the node N in the form of space separated integers.
Example
Sample Input 
14 

Sample Output 
1 2 5 14 

Explanation: The path to node 14 from root is 1 -> 2 -> 5 -> 14 (Explained in problem text) 

Sample Input 
999 

Sample Output 
1 2 6 16 44 125 353 999 
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static long nodes(int level)
	{
		if(level == 0)
			return 0l;
		long ans = 1l;
		int i;
		long ctr = 2l;
		for(i=2;i<=level;++i)
			{
				ans += ctr;
				if(i % 2 == 0)
					ctr *= 4;
				else	
					ctr *= 2;
			}
		return ans;
	}
	public static long solve(long n)
	{
		int level = 0;
		long st = 1;
		long ctr = 1;
		while(st <= n)
		{
			++level;
			st += ctr;
			if(level % 2 == 0)
				ctr *= 4;
			else
				ctr *= 2; 
		}

		long pre = nodes(level - 1);
		long remain = n - pre;
		long nodein = 0;
		if(level % 2 == 0)
			{
			nodein = remain / 2;
			if(remain % 2 > 0)
				++nodein;
			}
		else	
			{
			nodein = remain / 4;
			if(remain % 4 > 0)
				++nodein;
			}
	
		long nodeno = nodes(level - 2) + nodein;
	
		return nodeno;
	}
	public static void main (String[] args) {
		Scanner scan = new Scanner(System.in);
		ArrayList<Long> ans = new ArrayList<>();
		long n = scan.nextLong();
		ans.add(n);
		while(n > 1)
			{
				long secondlast = solve(n);
				ans.add(secondlast);
				n = secondlast;
			}
		for(int i = ans.size() - 1; i >= 0 ;--i)
			System.out.print(ans.get(i)  + " ");
	}
}