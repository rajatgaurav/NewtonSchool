/*
Problem Statement
Given a sting S of lowercase English character's. Your task is to find the minimum length subsequence which contains all characters from 'a' to 'z' in increasing order (i. e "abcdefghijklmnopqrstuvwxyz").
Input
Input contains a single string S. 

Constraints:- 
1 <= |S| <= 100000 

Note:- Sting will contain only lowercase english letters.
Output
Print the minimum length of such subsequence. If No such subsequence exist print -1.
Example
Sample Input:- 
aaaabcdefghijklmnopqrstuvwxyabcdzzz 

Sample Output:- 
30 

Sample Input:- 
abcd 

Sample Output:- 
-1
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	static int LCS(String S, int N, String T, int M, int dp[][]) {

		// Base Case
		if (N == 0 || M == 0)
		return 0;

		// Already Calculated State
		if (dp[N][M] != 0)
			return dp[N][M];

		// If the characters are the same
		if (S.charAt(N - 1)== T.charAt(M - 1)) {
			return dp[N][M] = 1 + LCS(S, N - 1, T, M - 1, dp);
		}

		// Otherwise
		return dp[N][M] = Math.max(LCS(S, N - 1, T, M, dp), LCS(S, N, T, M - 1, dp));
	}

	// Function to find the minimum number of
	// characters that needs to be appended
	// in the string to get all lowercase
	// alphabets as a subsequences
	static int minimumCharacter(String S) {

		// String containing all the characters
		String T = "abcdefghijklmnopqrstuvwxyz";

		int N = S.length(), M = T.length();

		// Stores the result of overlapping
		// subproblems
		int dp[][]= new int[N+1][M+1];
		// Return the minimum characters
		// required
		return (26 - LCS(S, N, T, M, dp));
	}

	// Driver Code
	public static void main (String[] args) {
                      // Your code here
		String S = "aaaabcdefghijklmnopqrstuvwxyabcdzzz";

		System.out.println(minimumCharacter(S));
	}
}