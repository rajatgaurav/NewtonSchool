/*
Problem Statement
Given an integer N find number of positive integers with N digits with all digits in non-decreasing order. As this can be large find ans modulo 1000000007. 
For example 111227 is valid whereas 1112231 is not.
Input
Input contains one line of input containing a single integer N. 

1 <= N <= 1000000000000
Output
Print a single integer containing the number of positive integers with N digits with all digits in non-decreasing order modulo 1000000007.
Example
Sample Input 
1 

Sample output 
9 

Sample Input 
2 

Sample Input 
45
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	static int nonDecNums(int n) 
    { 
        // a[i][j] = count of all possible number 
        // with i digits having leading digit as j 
        int[][] a = new int[n + 1][10]; 
  
        // Initialization of all 0-digit number 
        for (int i = 1; i <= 9; i++) 
            a[0][i] = 1; 
  
        // Initialization of all i-digit 
        // non-decreasing number leading with 9 
        for (int i = 1; i <= n; i++) 
            a[i][9] = 1; 
  
        // for all digits we should calculate 
        // number of ways depending upon leading 
        // digits 
        for (int i = 1; i <= n; i++) 
            for (int j = 8; j >= 0; j--) 
                a[i][j] = a[i - 1][j] + a[i][j + 1]; 
  
        return a[n][0]; 
    } 
  
    // driver program 
   	public static void main (String[] args) {
                      // Your code here
        int n = 1; 
        System.out.println(nonDecNums(n)); 
    } 
}