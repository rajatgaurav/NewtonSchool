/*
Problem Statement
Thomas liked a girl named Grace and he decided to reach out to her. Grace agreed to go on a date with him if he helped her solve a problem. The problem is: 

You are given an equation a - x = a ⊕ x (a xor x) and an integer parameter a. You have to tell how many non- negative solutions of this equation exist. 

Thomas is stuck in this problem and is asking you for your help. Help Thomas save his date.
Input
The first line of input contains a single integer T. Second line contains an integer a, parameter of the equation. 

Constraints:- 
1 <= T <= 100000 
1 <= a <= 2^60 -1
Output
For each value of a, print the number of non- negative solutions of the given equation.
Example
Sample Input:- 
3 
0 
2 
5 

Sample Output:- 
1 
2 
4
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework
import java.math.BigInteger;

// don't change the name of this class
// you can add inner classes if needed
class Main {
    public static long countValues(long n) {
        long count = 0;
        while (n > 0) {
            if ((n % 2) != 0)
                count++;
            n = n/2;
        }
        return (long) Math.pow(2, count);
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int T = input.nextInt();
        int arr[] = new int[T];
        for (int i = 0; i < T; i++) {
            long a = input.nextLong();
            System.out.println(countValues(a));
        }
    }
}