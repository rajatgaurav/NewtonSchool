/*
Problem Statement
Given a string s, find the first non- repeating character in the string and return its index. If it does not exist, return -1.
Input
First line of the input contains the string s. 

Constraints 
1<= s. length <= 100000
Output
Print the index of the first non- repeating character in a string
Example
Input 
s = "newtonschool" 

Output 
1 

Explanation 
"e" is the first non- repeating character in a string
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed

class CountIndex {
	int count, index;

	// constructor for first occurrence
	public CountIndex(int index)
	{
		this.count = 1;
		this.index = index;
	}

	// method for updating count
	public void incCount()
	{
		this.count++;
	}
}

class Main {
	static final int NO_OF_CHARS = 256;

	static HashMap<Character, CountIndex> map = new HashMap<Character, CountIndex>(NO_OF_CHARS);

	/* calculate count of characters
	in the passed string */
	static void getCharCountArray(String str) {
		for (int i = 0; i < str.length(); i++) {
			// If character already occurred,
			if (map.containsKey(str.charAt(i))) {
				// updating count
				map.get(str.charAt(i)).incCount();
			}

			// If it's first occurrence, then store
			// the index and count = 1
			else {
				map.put(str.charAt(i), new CountIndex(i));
			}
		}
	}

	/* The method returns index of first non-repeating
	character in a string. If all characters are repeating
	then returns -1 */
	static int firstNonRepeating(String str)
	{
		getCharCountArray(str);
		int result = Integer.MAX_VALUE, i;
		for (Map.Entry<Character, CountIndex> entry : map.entrySet())
		{
			int c=entry.getValue().count;
			int ind=entry.getValue().index;
			if(c==1 && ind < result)
			{
				result=ind;
			}
		}
		return result;
	}

	public static void main (String[] args) {
                      // Your code here
		Scanner input = new Scanner(System.in);
		String str = input.next();

		int index = firstNonRepeating(str);

		System.out.print(index == Integer.MAX_VALUE ? "-1" : index);
	}
}