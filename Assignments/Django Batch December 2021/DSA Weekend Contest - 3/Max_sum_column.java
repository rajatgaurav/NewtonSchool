/*
Problem Statement
Given a matrix of size M*N, your task is to find the maximum sum of a column.
Input
The first line of input contains two space- separated integers M and N. The next M lines of input contains N space- separated integers each depicting the values of the matrix. 

Constraints:- 
1 <= M, N <= 100 
1 <= Matrix[][] <= 100000
Output
Print the maximum sum between the columns.
Example
Sample Input:- 
3 3 
1 2 3 
4 5 6 
7 8 9 

Sample Output:- 
18 

Explanation:- 
1 + 4 + 7 = 12 
2 + 5 + 8 = 15 
3 + 6 + 9 = 18 
maximum = 18 

Sample Input:- 
3 2 
1 4 
9 6 
9 1 

Sample Output:- 
19
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework
// don't change the name of this class
// you can add inner classes if needed
class Main {
    public static void main (String[] args) {
                      // Your code here
        Scanner input = new Scanner(System.in);
        int rows = input.nextInt();
        int columns = input.nextInt();
        int matrix[][] = new int[rows][columns];
        for(int i = 0;i<rows;i++){
            for(int j = 0;j<columns;j++){
                matrix[i][j] = input.nextInt();
            }
        }
        int count = 0;
        int countMax = 0;
        for(int j = 0;j<columns;j++){
            count = 0;
            for(int i = 0;i<rows;i++){
                count = count + matrix[i][j];
            }
            if(count>countMax){
                countMax = count;
            }
        }
        System.out.print(countMax);
    }
}