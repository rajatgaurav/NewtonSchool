/*
Problem Statement
You are given an array of integers nums of size N, there is a sliding window of size K which is moving from the very left of the array to the very right. You can only see the K numbers in the window. Each time the sliding window moves right by one position. 
Print the max of K numbers for each position of sliding window
Input
1. The first line contains an integer N, denoting the size of the array. 
2. The second line has N space- separated integers of the array nums. 
3. The third line contains integer k, denoting the size of the sliding window 

Constraints : 
1 <= N <= 10^5 
10^-4 <= nums[i] <= 10^4 
1 <= K <= N
Output
Print the max of K numbers for each position of sliding window
Example
Sample Input:- 
8 
1 3 -1 -3 5 3 6 7 
3 

Sample Output:- 
3 3 5 5 6 7 

Explanation:- 
Window position Max 
- - - - 
[1 3 -1] -3 5 3 6 7 3 
1 [3 -1 -3] 5 3 6 7 3 
1 3 [-1 -3 5] 3 6 7 5 
1 3 -1 [-3 5 3] 6 7 5 
1 3 -1 -3 [5 3 6] 7 6 
1 3 -1 -3 5 [3 6 7] 7 

Sample Input:- 
1 
1 
1 

Sample Output:- 
1
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	static void printMax(int arr[], int n, int k) {
		Deque<Integer> Qi = new LinkedList<Integer>();
		
		for (int i = 0; i < k; ++i) {
			while (!Qi.isEmpty() && arr[i] >= arr[Qi.peekLast()])
				Qi.removeLast();

			Qi.addLast(i);
		}

		for (int i = k; i < n; ++i) {
			System.out.print(arr[Qi.peek()] + " ");

			while ((!Qi.isEmpty()) && Qi.peek() <= i - k)
				Qi.removeFirst();

			while ((!Qi.isEmpty()) && arr[i] >= arr[Qi.peekLast()])
				Qi.removeLast();

			Qi.addLast(i);
		}

		System.out.print(arr[Qi.peek()]);
	}

	// Driver Code
	public static void main (String[] args) {
                      // Your code here
		Scanner input = new Scanner(System.in);
		
		int n = input.nextInt();
		int[] arr = new int[n];
		for (int i = 0; i < n; i++) {
			arr[i] = input.nextInt();
		}
		int k = input.nextInt();

		printMax(arr, n, k);
	}
}