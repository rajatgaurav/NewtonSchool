/*
Problem Statement
Arpit Gupta has brought a toy for his valentine. She is playing with that toy which runs for T seconds when winded.If winded when the toy is already running, from that moment it will run for T seconds (not additional T seconds) For example if T is 10 and toy has run for 5 seconds and winded at this moment then in total it will run for 15 seconds. 
Arpit's Valentine winds the toy N times.She winds the toy at t[i] seconds after the first time she winds it.How long will the toy run in total?
Input
First Line of input contains two integers N and T 
Second Line of input contains N integers, list of time Arpit's Valentine has wound the toy at. 

Constraints 
1 <= N <= 100000 
1 <= T <= 1000000000 
1 <= t[i] <= 1000000000 
t[0] = 0
Output
Print a single integer the total time the toy has run.
Example
Sample input 1 
2 4 
0 3 

Sample output 1 
7 

Sample input 2 
2 10 
0 5 

Sample output 2 
15 

Explanation: 
Testcase1:- 
at first the toy is winded at 0 it will go till 4 but it again winded at 3 making it go for more 4 seconds 
so the total is 7
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework
// don't change the name of this class
// you can add inner classes if needed
class Main {
    public static void main (String[] args) {
                      // Your code here
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int t = input.nextInt();
        int[] arr = new int[n];
        for(int i=0; i<n; i++){
            arr[i] = input.nextInt();
        }
        long total = t;
        for(int i=1; i<n; i++){
            if((arr[i]-arr[i-1]) > t){
                total += t;
            }
            else{
                total += (arr[i] - arr[i-1]);
            }
        }
        System.out.println(total);
    }
}