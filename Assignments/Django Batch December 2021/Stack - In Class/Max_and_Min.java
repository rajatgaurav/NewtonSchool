/*
Problem Statement
Given an array Arr of N elements, your task is to find the sum of the difference of maximum and minimum element of all subarrays.
Input
The first line of input contains a single integer N, the next line of input contains N space separated integers depicting the values of the array. 

Constraints:- 
1 <= N <= 50000 
0 <= Arr[i] <= 100000
Output
Print the sum of the difference of all the possible subarrays.
Example
Sample Input:- 
4 
3 1 4 2 

Sample Output:- 
16 

Explanation:- 
Subarrays of size 1:- [3], [1], [4], [2], sum = 0 + 0 + 0 + 0 = 0 
Subarrays of size 2:- [3, 1], [1, 4], [4, 2], sum = 2 + 3 + 2 = 7 
Subarrays of size 3:- [3, 1, 4], [1, 4, 2], sum = 3 + 3 = 6 
Subarrays of size 4:- [3, 1, 4, 2], sum = 3 
Total sum = 16 

Sample Input:- 
4 
5 2 0 6 

Sample Output:- 
28
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[] arr = new int[n];
        for (int i =0;i< n;i++) {
            arr[i] = input.nextInt();
        }
        System.out.println(getSumOfDiff(arr,n));
	}

    private static long getSumOfDiff(int[] arr, int n){
        long sumOfMax = sumMax(arr,n);
        long sumOfMin = sumMin(arr, n);
        return sumOfMax - sumOfMin;
    }

     private static long sumMin(int[] arr, int n){
        long[] left = new long[n], right = new long[n];
        Stack<Integer> stack = new Stack<>();
        
        for (int i =0;i<n;i++) {
            while (!stack.isEmpty() && arr[stack.peek()] > arr[i]) {
                left[i] += left[stack.peek()] + 1;
                stack.pop();
            }
            stack.push(i);
        }

         stack = new Stack<>();

        for (int i = n-1;i>=0;i--) {
             while (!stack.isEmpty() && arr[stack.peek()] >= arr[i]) {
                right[i] += right[stack.peek()] + 1;
                stack.pop();
            }
            stack.push(i);
        }

        long result = 0;
        for (int i =0;i<n;i++) {
            result += (left[i] + 1) * (right[i] + 1) * arr[i];
        }
        return result;
     }

    private static long sumMax(int[] arr, int n){
        long[] left= new long[n], right = new long[n];
        Stack<Integer> stack = new Stack<>();

        for (int i =0;i<n;i++) {
            while (!stack.isEmpty() && arr[stack.peek()] <= arr[i]) {
                left[i] += left[stack.peek()] + 1;
                stack.pop();
            }
            stack.push(i);
        }
        stack = new Stack<>();

        for (int i = n-1;i>=0;i--) {
            while(!stack.isEmpty() && arr[stack.peek()] < arr[i]){
                right[i] += right[stack.peek()] + 1;
                stack.pop();
            }
            stack.push(i);
        }

        long result = 0;
        for (int i =0;i<n;i++) {
            result += (left[i] + 1) * (right[i] + 1) * arr[i];
        }
        return result;
    }
}