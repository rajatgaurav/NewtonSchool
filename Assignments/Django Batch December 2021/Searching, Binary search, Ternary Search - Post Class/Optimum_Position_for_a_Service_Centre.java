/*
Problem Statement
A delivery firm want to establish a new service centre in a different city. The corporation knows the positions of all the clients in this city on a 2D- Map and want to develop the new centre in such a way that the total of the euclidean distances to all customers is as small as possible. 
Return the least sum of the euclidean distances to all customers given an array positions where positions[i] = (xi, yi) is the position of the ith client on the map. 
In other words, the position of the service centre (xcentre, ycentre) must be chosen so that the following formula is minimised: 
Formula 
Answers within 10^-5 of the actual value will be accepted.
Input
The first line contains n (the number of customers) 
Next n lines contains coordinates of customers (xi,yi). 

Constraints 
1 <= n <= 50 
0 <= xi, yi <= 100
Output
Output the minimum sum of the euclidean distances to all customers.
Example
Input 
4 
0 1 
1 0 
1 2 
2 1 

Output 
4.00000 

Explanation : 
As shown, you can see that choosing (xcentre, ycentre) = [1, 1] will make the distance to each customer = 1, the sum of all distances is 4 which is the minimum possible we can achieve.
*/


// C++ Code

#include <bits/stdc++.h>
#define Lim 0.999999
using namespace std;
const double eps = 1e-8; 
const double delta = 0.98;
const double T = 100; 
const double INF = 999999999;
const int N = 105;
struct Point {
    double x,y;
}
p[N];
int n;
int dir[][2] = {{-1,0},{1,0},{0,1},{0,-1}};
double dis(Point a,Point b)
{
    return sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y));
}
double getSum(Point p[],Point s){
    double ans = 0;
    for(int i=0;i<n;i++){
        ans += dis(p[i],s);
    }
    return ans;
}
double Search(Point p[]){
    Point s = p[0]; 
    double res = INF;
    double t = T;
    while(t>eps){
        bool flag = true;
        while(flag){
            flag = false;
            for(int i=0;i<4;i++){
                Point next;
                next.x = s.x+dir[i][0]*t;
                next.y = s.y+dir[i][1]*t;
                double ans = getSum(p,next);
                if(ans<res){
                    res = ans;
                    s = next;
                    flag = true;
                }
            }
        }
        t = t*delta;
    }
    return res;
}
int main(){
    while(scanf("%d",&n)!=EOF){
        for(int i=0;i<n;i++){
            scanf("%lf%lf",&p[i].x,&p[i].y);
        }
        printf("%.5lf\n",Search(p));
    }
}