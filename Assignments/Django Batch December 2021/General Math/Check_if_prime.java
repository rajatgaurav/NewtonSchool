/*
Problem Statement
Given an integer, print whether that integer is a prime number or not.
Input
First line of input contains an integer T, showing the number of test cases. Every test case is a single integer A. 

Constraints 
1 <= T <= 100 
1 <= A <= 10^8
Output
If the given integer is prime, print 'Yes', else print 'No'.
Example
Sample Input 
3 
5 
9 
13 

Output 
Yes 
No 
Yes
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
		Scanner input = new Scanner(System.in);
		
		int T = input.nextInt();
		int[]A= new int[T];
			
		for (int i=0;i<T;i++){
			A[i]=input.nextInt();
		}
		for (int i = 0; i < T; i++){
			int count = 0;
			for(int j = 2; j <= Math.sqrt(A[i]);j++){
				if(A[i]%j==0){
					count = count+1;
				}
			}
			if(count == 0){
				System.out.printf("Yes\n");
			} else {
				System.out.printf("No\n");
			}
		}
	}
}