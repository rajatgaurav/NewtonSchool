/*
Problem Statement
Given a number n find the number of prime numbers less than equal to that number.
Input
There is only one integer containing value of n. 

Constraints:- 
1 <= n <= 10000000
Output
Return number of primes less than or equal to n
Example
Sample Input 
5 

Sample Output 
3 

Explanation:- 
2 3 and 5 are the required primes. 

Sample Input 
5000 

Sample Output 
669
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {

    static int countPrimes(int n) {
        boolean prime[] = new boolean[n];
        for(int i=2 ; i<n ; i++){
            prime[i] = true;
        }
        int cnt = 0;
        for(int i=2 ; i<n ; i++){
            if(prime[i]){
                cnt++;
                for(int j = 2*i ; j<n ; j=j+i){
                    prime[j] = false;
                }
            }
        }
        return cnt;
    }
 
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        System.out.print(countPrimes(n));
    }
}