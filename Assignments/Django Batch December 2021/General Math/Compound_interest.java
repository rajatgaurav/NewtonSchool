/**
Problem Statement
Write a program to find the compound interest for given principal amount P, time Tm(in years), and interest rate R.
Input
The input contains three integers P, R, and Tm. 

Constraints:- 
1 < = P < = 10^3 
1 < = R < = 100 
1 < = Tm < = 20
Output
Print the compound interest by 2 decimal places.
Example
Sample Input: 
100 1 2 

Sample Output:- 
2.01 

Sample Input: 
1 99 2 

Sample Output:- 
2.96 
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
		Scanner input = new Scanner(System.in);
        double p = 0.0, r = 0.0, t = 0.0, amount = 0.0, compoundInterest = 0.0;
        p = input.nextDouble();
        r = input.nextDouble();
        t = input.nextDouble();
        r = r / 100;
        amount = p * Math.pow(1+(r), t);
        compoundInterest = amount - p;

        System.out.printf("%.2f", compoundInterest);
    }
}