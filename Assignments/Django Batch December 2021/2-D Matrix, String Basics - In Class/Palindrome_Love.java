/*
Problem Statement
Tom loves palindromes. He has the power to convert any ordinary string to a palindrome. 
In one move tom can choose a character of a string and change it to any other character. 
Given a string, find the minimum number of moves in which tom can change it to a palindrome.
Input
Input consists of a string. 
Every character of a string contains lowercase alphabets 'a' to 'z' inclusive. 

Constraints 
1 <= |s| <= 1000
Output
The minimum number of moves to convert the given string to a palindrome.
Example
Sample input 1 
naman 

Sample output 1 
0 

Sample input 2 
reorder 

Sample output 2 
1
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
		Scanner input=new Scanner(System.in);
		String str=input.next();
		int size = str.length();
		char[] arr=str.toCharArray();
		String revStr = "";
		for(int i=0; i<size; i++){
			revStr=str.charAt(i)+revStr;
		}
		char[] revArr=revStr.toCharArray();

		int moves=0;
		for(int i=0; i<size; i++){
			if(arr[i]!=revArr[i]){
				moves++;
			}
		}
		System.out.print(moves/2);
                      // Your code here
	}
}