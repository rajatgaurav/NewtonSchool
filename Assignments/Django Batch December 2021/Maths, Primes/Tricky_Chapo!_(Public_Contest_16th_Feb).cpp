/*
Problem Statement
As always, Sid has come up with a new problem on Number Theory. Since it is too tricky to solve, can you help him. A bonus, you'll get a chapo for solving it right. 
Given an integer n, let F(n) be the product of all positive integers i less or equal than n such that n and i are co-prime. 
Now you're given an integer X, you need to find the sum of (F(i) modulo i) for all positive integers i less than or equal to X. If the answer is A, report the value of (A % 1000000007).
Input
The only line of input contains a single integer X. 

Constraints 
2 <= X <= 500000000
Output
Output a single integer, the required output for the problem.
Example
Sample Input 
5 

Sample Output 
10 

Sample Input 
1000 

Sample Output 
128825
*/

#include<bits/stdc++.h>
using namespace std;

int gcd(int a, int b)
{
    if (a == 0)
        return b;
    return gcd(b % a, a);
}
int main(){
    int sum=0;
    int x;
    cin>>x;
    for (int i=1;i<=x;i++){
        int f=1;
        for (int j=1;j<=i;j++){
            if(i%j==0) continue;
            if(gcd(j,i)==1)
                f*=j;
                f=f%i;
        }
        sum+=(f%i);
    }
    cout<<(sum%1000000007);
}