/*
Problem Statement
Given a binary tree, with root 1, print the postorder traversal of the tree. 
Algorithm Postorder(tree) 
1. Traverse the left subtree 
2. Traverse the right subtree 
3. Visit the root
Input
The input consists of N+1 lines. 
First line contains the integer N, denoting the number of nodes in the binary tree. 
Next N lines contains two integers denoting the left and right child of the i'th node respectively. 
If the node doesn't have a left or right child, it is denoted by '-1' 

1 <= N <= 100000
Output
Print a single line containing N space separated integers representing the postorder traversal of the given tree
Example
Sample Input 1: 
5 
2 4 
5 3 
-1 -1 
-1 -1 
-1 -1 
Sample output 1: 
5 3 2 4 1 

Explanation: Given binary tree 
1 
/ \ 
2 4 
/ \ 
5 3 
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        TreeNode[] arr = new TreeNode[n];
        for(int i =0; i <n;i++){
            arr[i] = new TreeNode(i + 1);
        }
        for(int i = 0;i<n;i++){
            int left = input.nextInt();
            int right = input.nextInt();
            if(left != -1){
                arr[i].left = arr[left -1];
            }
            if(right != -1){
                arr[i].right = arr[right - 1];
            }
        }
        postOrderConstantspace(arr[0]);
	}

    TreeNode root;
 
    // Function to find Post Order
    // Traversal Using Constant space
    static void postOrderConstantspace(TreeNode root)
    {
        if (root == null)
            return;
 
        TreeNode current
            = new TreeNode(-1),
            pre = null;
        TreeNode prev = null,
                succ = null,
                temp = null;
        current.left = root;
 
        while (current != null) {
 
            // Go to the right child
            // if current does not
            // have a left child
 
            if (current.left == null) {
                current = current.right;
            }
 
            else {
 
                // Traverse left child
                pre = current.left;
 
                // Find the right most child
                // in the left subtree
                while (pre.right != null
                    && pre.right != current)
                    pre = pre.right;
 
                if (pre.right == null) {
 
                    // Make current as the right
                    // child of the right most node
                    pre.right = current;
 
                    // Traverse the left child
                    current = current.left;
                }
 
                else {
                    pre.right = null;
                    succ = current;
                    current = current.left;
                    prev = null;
 
                    // Traverse along the right
                    // subtree to the
                    // right-most child
 
                    while (current != null) {
                        temp = current.right;
                        current.right = prev;
                        prev = current;
                        current = temp;
                    }
 
                    // Traverse back from
                    // right most child to
                    // current's left child node
 
                    while (prev != null) {
 
                        System.out.print(prev);
                        temp = prev.right;
                        prev.right = current;
                        current = prev;
                        prev = temp;
                    }
 
                    current = succ;
                    current = current.right;
                }
            }
        }
    }
}

class TreeNode {
    public int data;
    public TreeNode left;
    public TreeNode right;
    public TreeNode(int data)
    {
        this.data = data;
    }
 
    public String toString()
    {
        return data + " ";
    }
}


/*
	public static void main (String[] args) {
                      // Your code here
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        Node[] arr = new Node[n];
        for(int i =0; i <n;i++){
            arr[i] = new Node(i + 1);
        }
        for(int i = 0;i<n;i++){
            int left = input.nextInt();
            int right = input.nextInt();
            if(left != -1){
                arr[i].left = arr[left -1];
            }
            if(right != -1){
                arr[i].right = arr[right - 1];
            }
        }
        printPostorder(arr[0]);
	}

    static Node root;
 
    Main() { root = null; }
 
    
    void printPostorder(Node node)
    {
        if (node == null)
            return;
 
        // first recur on left subtree
        printPostorder(node.left);
 
        // then recur on right subtree
        printPostorder(node.right);
 
        // now deal with the node
        System.out.print(node.key + " ");
    }
 
    // Wrappers over above recursive functions
    static void printInorder() { printInorder(root); }
}

class Node {
    int key;
    Node left, right;
 
    public Node(int item)
    {
        key = item;
        left = right = null;
    }
}
*/