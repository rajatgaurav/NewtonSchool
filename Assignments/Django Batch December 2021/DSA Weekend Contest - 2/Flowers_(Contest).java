/*
Problem Statement
The flower shop near his house sells flowers of N types. It is found that the store has Ai flowers of the i-th type. We like odd numbers. Therefore, we have decided that there should be an odd number of flowers of each type in the bouquet, and the total number of flowers in the bouquet should also be odd. 
Determine the maximum number of flowers the bouquet can consist of.
Input
The first line contains an integer N — the number of types of flowers that are sold in the store 
The second line contains N integers— the number of flowers of each type 

1 <= N <= 100000 
1 <= Ai <= 1000
Output
Print one number — the maximum number of flowers the bouquet can consist of.
Example
Sample input 
3 
3 5 8 

Sample output 
15 

Sample input 
3 
1 1 1 

Sample output 
3
*/


// C++ Code

#include <bits/stdc++.h> // header file includes every Standard library
using namespace std;
int main() {
    int n;
    cin>>n;
    int min,s=0;
    int a[n];
    for(int i=0;i<n;i++)
    {
        cin>>a[i];
        if(a[i]%2==0)
        {
            a[i]=a[i]-1;
        }
        s+=a[i];
    }
    min=a[0];
    for(int i=1;i<n;i++)
    {
        if(a[i]<min)
        min=a[i];
    }
    if(n%2==0)
    cout<<s-min;
    else
    cout<<s;
    // Your code here
    return 0;
}