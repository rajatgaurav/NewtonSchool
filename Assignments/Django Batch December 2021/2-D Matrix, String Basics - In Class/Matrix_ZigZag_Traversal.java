/*
Problem Statement
Given a 2d matrix of size M*N, print the zig traversal of the matrix as shown:- 

Consider a matrix of size 5*4 
1 2 3 4 
5 6 7 8 
9 10 11 12 
13 14 15 16 
17 18 19 20 

ZigZag traversal:- 

1 
5 2 
9 6 3 
13 10 7 4 
17 14 11 8 
18 15 12 
19 16 
20
Input
First line of input contains two integers M and N. Next M lines contains N space- separated integers each. 

Constraints:- 
1 <= M, N <= 100 
1 <= Matrix[i][j] <= 100000
Output
Print the zig- zag traversal of the matrix as shown.
Example
Sample Input:- 
4 3 
1 2 3 
4 5 6 
7 8 9 
10 11 12 

Sample Output:- 
1 
4 2 
7 5 3 
10 8 6 
11 9 
12
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	    public static int R, C;

	private static void diagonalOrder(int[][] arr){
		for (int k = 0; k < R; k++) {
			System.out.print(arr[k][0] + " ");

			int i = k - 1;
			int j = 1;

			while (isValid(i, j)){
				System.out.print(arr[i][j] + " ");
				i--;
				j++;
			}
			System.out.println("");
		}

		for (int k = 1; k < C; k++){
			System.out.print(arr[R - 1][k] + " ");

			int i = R - 2;
			int j = k + 1;

			while (isValid(i, j)){
				System.out.print(arr[i][j] + " ");

				i--;
				j++;
			}
			System.out.println("");
		}
	}

	public static boolean isValid(int i, int j){
		if (i < 0 || i >= R
			|| j >= C || j < 0)
			return false;
		return true;
	}

	public static void main(String[] args){
					// Your code here
		Scanner input = new Scanner(System.in);
		int row = input.nextInt();
		int column = input.nextInt();

		int [][] arr = new int[row][column];
		for (int i = 0; i < row; i++){
    		for(int j = 0; j < column; j++) {
    			arr[i][j] = input.nextInt(); 
			}
		}

		R = arr.length;
		C = arr[0].length;

		diagonalOrder(arr);
	}
}

/**
another logic

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
		Scanner inputTaker = new Scanner(System.in);
		int row = inputTaker.nextInt();
		int column = inputTaker.nextInt();
		int[][] metric = new int[row][column];

		for(int i =0;i<row;i++){
			for(int j =0;j<column;j++){
				metric[i][j] = inputTaker.nextInt();
			}
		}


		for(int j =0;j<row;j++){
			for(int i =j;i>=0;i--){
				if((j-i) >= column){
					break;
				}
				System.out.print(metric[i][j-i] + " ");
			}
			System.out.println("");
		}

		for(int j = 1;j< column;j++){
			for(int i = row -1;i>=0;i--){
				if((j + row -1 -i) >= column){
					break;
				}
				System.out.print(metric[i][j + (row -1 -i)] + " ");
			}
			System.out.println("");
		}		
	}
}


*/