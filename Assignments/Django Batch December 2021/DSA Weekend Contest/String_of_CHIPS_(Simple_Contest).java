/*
Problem Statement
A string is a string of CHIPS if it is a string formed by concatinating "chips" a positive number of times to an empty string. For example "chips" and "chipschips" are string of CHIPS whereas "chipsch" and "random" are not. Find out minimum number of moves required to make a given string S into a string of CHIPS. In a single move you can set any character of string to any character.
Input
First and only line of input contains a single string S. 

Constraints: 
1 <= |S| <= 100000 
S contains lowercase english letters. 
|S| is a multiple of 5.
Output
Print the minimum number of moves required to make string S into a string of CHIPS.
Example
Sample input 1 
chips 

Sample output 1 
0 

Sample input 2 
ohipo 

Sample output 2 
2
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework
// don't change the name of this class
// you can add inner classes if needed
class Main {
    public static void main (String[] args) {
        String s = "chips";
        Scanner input = new Scanner(System.in);
        String str = input.next();
        int count = 0;
        for (int i = 0; i < str.length(); i++){
            if (str.charAt(i) != s.charAt(i % 5)) count++;
        }
        System.out.println(count);
    }
}