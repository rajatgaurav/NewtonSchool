/*
Problem Statement
Given an array A of N integers, find whether there exists three consecutive same integers in the array.
Input
The first line of the input contains an integer N. 
The second line contains N space separated integers of the array A. 

Constraints 
3 <= N <= 1000 
1 <= A[i] <= 100
Output
Output "Yes" if there exists three consecutive equal integers in the array, else output "No" (without quotes).
Example
Sample Input 
5 
1 2 2 2 4 

Sample Output 
Yes 

Explanation: The segment [2, 2, 2] follows the criterion. 

Sample Input 
5 
1 2 2 3 4 

Sample Output 
No
*/

import java.io.*; // for hAndling input/output
import java.util.*; // contAins Collections frAmework

// don't chAnge the nAme of this clAss
// you cAn Add inner clAsses if needed
class Main {
	public static void main (String[] args) {
        // Your code here
		Scanner input =new Scanner (System.in);
		int N = input.nextInt();
		int count = 0;
		int A[] = new int[N];
		for (int i = 0; i < N; i++) {
			A[i] = input.nextInt();
		}
		  for (int i = 0; i < N - 2; i++) {
	    if(A[i]==A[i+1] && A[i+1]==A[i+2]){
	        System.out.print("Yes");
	        return;
	    }
	}
	System.out.print("No");
	}	
}