/*
Problem Statement
The universe contains a magic number z. Thor's power is known to x and Loki's power to be y. 

One's strength is defined to be z - a, if his power is a. Your task is to find out who among Thor and Loki has the highest strength, and print that strength. 

Note: The input and answer may not fit in a 32-bit integer type. In particular, if you are using C++ consider using long long int over int.
Input
The first line contains one integer t — the number of test cases. 
Each test case consists of one line containing three space-separated integers x, y and z. 

Constraints: 
1 ≤ t ≤ 104 
1 ≤ x, y ≤ 1015 
max(x, y) < z ≤ 1015
Output
For each test case, print a single value - the largest strength among Thor and Loki.
Example
Sample Input 
2 
2 3 4 
1 1 5 

Sample Output 
2 
4
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework
// don't change the name of this class
// you can add inner classes if needed
class Main {
    public static void main (String[] args) {
                      // Your code here
        Scanner input = new Scanner(System.in);
        int t = input.nextInt();
        while(t-->0){
            long x = input.nextLong();
            long y = input.nextLong();
            long z = input.nextLong();
            if(z - x > z - y){
                System.out.println(z - x);
            } else 
                System.out.println(z - y);
            }
        }
}