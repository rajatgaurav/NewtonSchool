/*
Problem Statement
Given a string S, check if the given string contains any white space or not.
Input
Input contains a single string S. 

Constraints:- 
1 < = |S| < = 20 


Note:- String will only contain lowercase english letters.
Output
Print "Yes" if the given string contains a whitespace else print "No"
Example
Sample Input:- 
newton school 

Sample Output:- 
Yes 

Sample Input:- 
newtonschool 

Sample Output:- 
No
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
		Scanner input = new Scanner(System.in);
		String str = input.nextLine();

		if(str.contains(" ")){
			System.out.print("Yes");
		} else{
			System.out.print("No");
		}
	}
}