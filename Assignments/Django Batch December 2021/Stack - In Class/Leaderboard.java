/*
Problem Statement
Peter Parker is taking part in a competition where Mr Tony Stark has come to see Peter lift the cup. But Tony can't tell what position Peter is on since the school authorities didn't display a leaderboard. Help Tony Identify which rank is Peter on by seeing his victory status in different rounds. Points are awarded on solving every question based on the difficulty level of the question which are divided into subcategories. The player with the highest score is ranked number 1 on the leaderboard. Players who have equal scores receive the same ranking number, and the next player(s) receive the immediately following ranking number.
Input
The first line contains an integer n, the number of participants on the leaderboard. The next line contains n space- separated integers containing the leaderboard points in decreasing order in the competition. The next line contains an integer m, denoting the number of times Peter is attempting the question. The last line contains m space- separated integers containing Peter's score in each attempt in the competition. 

Constraints:- 
1 <= n <= 100000 
1 <= m <= 1000 
1 <= points, score <= 10000000 

Note:- 
In the existing leaderboard, points are in descending order. Peter's points are in ascending order.
Output
Print m integers indicating Peter's rank in each competition.
Example
Sample Input:- 
7 
100 100 50 40 40 20 10 
4 
5 25 50 120 

Sample Output:- 
6 
4 
2 
1
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();

        Stack<Integer> stack = new Stack<>();
        for (int i =0;i< n;i++) {
            int x = input.nextInt();
            if (stack.isEmpty() || stack.peek() != x) {
                stack.push(x);
            }
        }

        int m = input.nextInt();
        for (int i =0;i< m;i++) {
            int x = input.nextInt();
            while (!stack.isEmpty() && stack.peek() <= x) {
                stack.pop();
            }
            System.out.println(stack.size() + 1);
        }
	}
}