/*
Problem Statement
Given a linked list consisting of N nodes and two integers M and K. Your task is to add element K at the Mth position from the start of the linked list
Input
User Task: 
Since this will be a functional problem, you don't have to take input. You just have to complete the function addElement() that takes head node, M(position of element to be inserted) and K(the element to be inserted) as parameter. 

Constraints: 
1 <= M <=N <= 1000 
1 <=K, Node.data<= 1000
Output
Return the head of the modified linked list
Example
Sample Input:- 
5 3 2 
1 3 2 4 5 

Sample Output:- 
1 3 2 2 4 5 

Explanation:- 
here M is 3 and K is 2 
so we insert 2 at the 3rd position, resulting list will be 1 3 2 2 4 5 

Sample Input 2:- 
5 2 6 
1 2 3 4 5 

Sample Output 2:- 
1 6 2 3 4 5 
*/

static class Node {
    Node next;
    int val;

    Node(int val) {
        this.val = val;
        next = null;
    }
}
    // function to create and return a Node
    static Node GetNode(int val) {
        return new Node(val);
    }
 
    // function to insert a Node at required pos
    public static Node addElement(Node head, int val, int pos) {
    //enter your code here
        Node head1 = head;
        if (pos < 1)
            System.out.print("Invalid pos");
 
        // if pos is 1 then new node is
        // set infornt of head node
        // head node is changing.
        if (pos == 1) {
            Node newNode = new Node(val);
            newNode.next = head;
            head1 = newNode;
        } else {
            while (pos-- != 0) {
                if (pos == 1) {
                    // adding Node at required pos
                    Node newNode = GetNode(val);
 
                    // Making the new Node to point to
                    // the old Node at the same pos
                    newNode.next = head.next;
 
                    // Replacing current with new Node
                    // to the old Node to point to the new Node
                    head.next = newNode;
                    break;
                }
                head = head.next;
            }
            if (pos != 1)
                System.out.print("Position out of range");
        }
        return head1;
    }