/*
Problem Statement
Given a string S of length N. Find a string R of length N such that hamming distance between S and R is 1 and R is lexicographically smallest possible. 
Note: The Hamming distance between two strings of equal length is the number of positions at which the corresponding symbols are different.
Input
Input contains a single string S containing english lowercase alphabets. 

Constraints: 
1 <= |S| <= 100000
Output
Print a single string R, such that R contains english lowercase letters only.
Example
Sample Input 
aba 

Sample Output 
aaa 

Explanation: hamming distance between aba and aaa = 1 and aaa is lexicographically smallest such string. 

Sample Input 
a 

Sample Output 
b
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
    static void findString(String str, int n, int k) {
        if (k == 0) {
            System.out.println(str);;
            return;
        }
    String str2 = str;
    int p = 0;
    for (int i = 0; i < n; i++) {
        if (str2.charAt(i) != 'a') {
            str2 = str2.substring(0,i)+'a'+str2.substring(i+1);
            p++;
            if (p == k)
                break;
        }
    }
    if (p < k) {
        for (int i = n - 1; i >= 0; i--)
            if (str.charAt(i) == 'a') {
                str2 = str2.substring(0,i)+'b'+str2.substring(i+1);
                p++;
                if (p == k)
                    break;
            }
        }
        System.out.println(str2);
    }
 
 public static void main(String[] args) {
                    // Your code here
    Scanner input = new Scanner(System.in);
	String str = input.nextLine();
    int n = str.length();
    int k = 1;
 
    findString(str, n, k);
 }
}