/*
Problem Statement
Given a tree with N nodes, 

Saloni wants to put weights on all edges of the tree so that all weights are non-negative real numbers and their sum is s. At the same time, she wants to make the diameter of the tree as small as possible. 

Let's define the diameter of a weighed tree as the maximum sum of the weights of the edges lying on the path between two some vertices of the tree. In other words, the diameter of a weighed tree is the length of the longest simple path in the tree, where length of a path is equal to the sum of weights over all edges in the path. 

Find the minimum possible diameter that Saloni can get.
Input
The first line contains two integer numbers n and s, the number of vertices in the tree and the sum of edge weights. 

Each of the following n−1 lines contains two space-separated integer numbers ai and bi (1≤ ai, bi ≤n, ai≠bi) — the indexes of vertices connected by an edge. The edges are undirected. 

It is guaranteed that the given edges form a tree. 

Constraints 
1 <= n <= 100000 
1 <= s <= 1000000000
Output
If the minimum diameter of the tree that Vanya can get by placing some non-negative real weights on its edges with the sum equal to s is d, print floor(d).
Example
Sample Input 
4 3 
1 2 
1 3 
1 4 

Sample Output 
2 

Sample Input 
5 5 
1 2 
2 3 
3 4 
3 5 

Sample Output 
3 

Explanation: 
In the first sample, we place 1 weight on each of the given edges. 
In the second sample, we place weights 1.6666... on edges (1, 2), (3, 4), (3, 5). Therefore, minimum diameter achieved is equal to 3.333...
*/

#include <bits/stdc++.h>
#define ios ios_base::sync_with_stdio(false);cin.tie(NULL);cout.tie(NULL);
#define ll long long
#define bit(x,j) ((x>>j)&1)
using namespace std;
// bitset<8>set1(p);
// __builtin_popcountll(x);
// bitset<100> b("1010");
ll int mod=1e9+7;//998244353;
int mul(ll int x, ll int y)
{
    return (x * 1ll * y) % mod;
}

int add(int x, int y)
{
    x += y;
    while(x >= mod) x -= mod;
    while(x < 0) x += mod;
    return x;
}

long long power(long long a, long long b,ll m) {
    a %= m;
    long long res = 1;
    while (b > 0) {
        if (b & 1)
            res = mul(res,a);
        a = mul(a,a);
        b >>= 1;
    }
    return res%m;
    }


  ll int binomial(ll int n, ll int k) 
{ 
    ll int C[k+1]; 
    memset(C, 0, sizeof(C)); 
  
    C[0] = 1;  // nC0 is 1 
  
    for (ll int i = 1; i <= n; i++) 
    { 
        // Compute next row of pascal triangle using 
        // the previous row 
        for (int j = min(i, k); j > 0; j--) 
            C[j] = C[j]+C[j-1]; 
    } 
    return C[k]; 
} 
  vector<int> pr;
  bool prime[10000000];
void Sieve(ll int n) 
{ 
    memset(prime, true, sizeof(prime)); 
    prime[1]=false;
  
    for (int p=2; p*p<=n; p++) 
    { 
        if (prime[p] == true) 
        { 
            pr.push_back(p);
            for (int i=p*p; i<=n; i += p) 
                prime[i] = false; 
        } 
    } 
}
 


//It reurns Fn,Fn+1
pair<int, int> fib (int n) {
    if (n == 0)
        return {0, 1};

    auto p = fib(n >> 1);
    int c = p.first * (2 * p.second - p.first);
    int d = p.first * p.first + p.second * p.second;
    if (n & 1)
        return {d, c + d};
    else
        return {c, d};
}
int sum(int n)  
{  
    if (n == 0)  
    return 0;  
    return (n % 10 + sum(n / 10));  
}  
ll int factorial(ll int n,ll int m) 
{ 
    ll int fact = 1; 
    for(int i=2;i<=n;i++) { 
        fact = (fact * i)%m; 
    } 
    return fact%m; 
} 

int reduce(int x, int y) 
{ 
    int d; 
    d = __gcd(x, y); 
  
    x = x / d; 
    y = y / d; 
    return x; 
} 


int main() {
  ios;
  ll int n;
  cin>>n;
  double s;
  cin>>s;
  map<int,int>mp,ma;
  for(int i=0;i<n-1;i++)
  {
      int a,b;
      cin>>a>>b;
      mp[a]++;
      mp[b]++;
  }
  ll int l=0;
   for(int i=1;i<=n;i++)
   {
       if(mp[i]==1)
       l++;
   }
  // cout<<l<<endl;
   long long k=2*(s)/(double)l;
   std::cout << std::setprecision(9) << k << '\n';
}