/*
Problem Statement
Given an array A[], of length N containing values in the range of negative to positive integers. You need to find the length of the largest subarray whose sum of elements is 0.
Input
The first line contains N denoting the size of the array A. Then in the next line contains N space-separated values of the array A. 

Constraints:- 
1 <= N <= 1e5 
-1e6 <= A[i] <= 1e6
Output
Print the length of the largest subarray which has sum 0, If no subarray exist print -1.
Example
Sample Input:- 
8 
15 -2 2 -8 1 7 10 23 

Sample Output:- 
5 

Explanation:- 
-2 2 -8 1 7 is the required subarray 

Sample Input:- 
5 
1 2 1 2 3 

Sample Output:- 
-1
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
		Scanner input = new Scanner(System.in);
		int n = input.nextInt();
		int[] arr = new int[n];
		for(int i = 0;i< n;i++){
			arr[i] = input.nextInt();
		}
		System.out.println(getLargestSubArray(arr, n));
	}

	private static long getLargestSubArray(int[] arr, int n){
		long result = -1;
		long cumSum = 0;
		HashMap<Long, Integer> map = new HashMap<>();

		for(int i = 0;i< n;i++){
			cumSum += arr[i];

			if(cumSum == 0){
				result = i + 1;
			}

			if(map.containsKey(cumSum)){
				result = Math.max(result, i - map.get(cumSum));
			}else{
				map.put(cumSum, i);
			}
		}

		return result;
	}
}