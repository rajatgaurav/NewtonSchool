/*
Problem Statement
You are given a String S and two empty strings A and B. You are allowed to perform following two operations. 
1. Remove an element from start of S and add it to the end of B. 
2. Remove an element from end of B and add it to the end of A. 

Find the lexicographical minimum string A that we can obtain using the above procedure. 

Note: The final length of string A must be equal to initial length of string S.
Input
Input contains string S containing only lowercase alphabets. 

Constraints 
1 <= |S| <= 100000
Output
Print the lexicographical minimum string A that we can obtain.
Example
Input 
cab 

Output 
abc 

Input 
acdb 

Output 
abdc
*/

#include <bits/stdc++.h>
using namespace std;
const int N = 2e6 + 20;
string s, t, u, a;
 
int b[30];
int pos[30];
int h[N];
stack<char> sta;
int main()
{
    while (cin >> s)
    {
        int len = s.length();
        memset(b, 0, sizeof(b));
 
        for (int i = 0; s[i]; i++)
            b[s[i] - 'a']++;
        int i = 0, k;
        while (s[i])
        {
            int k1 = 26;
            if (!sta.empty())
                k1 = sta.top() - 'a';
            for (k = 0; k < 26; k++)
            {
                if (b[k])
                    break;
            }
            k = min(k1, k);
            while (true)
            {
                if (!sta.empty() && sta.top() - 'a' == k)
                    break;
                b[s[i] - 'a']--;
                sta.push(s[i++]);
                if (s[i] == '\0')
                    break;
            }
            if (!sta.empty())
            {
                printf("%c", sta.top());
                sta.pop();
            }
        }
        while (!sta.empty())
        {
            char c = sta.top();
            sta.pop();
            printf("%c", c);
        }
        cout << endl;
    }
    return 0;
}















/**
import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static String stringStack(String S) {
		int N = S.length();
		int[] suff = new int[N + 1];

		suff[N] = Integer.MAX_VALUE;

		for (int i = N - 1; i >= 0; i--) {
			suff[i] = Math.min(suff[i + 1], (int)S.charAt(i));
		}

		Stack<Character> A = new Stack<Character>();
		String B = "";

		for (int i = 0; i < N; i++) {
			if (A.size() > 0 && suff[i] >= A.peek()) {
				B += A.peek();
				A.pop();
				i--;
			} else if (suff[i] == S.charAt(i)) {
				B += S.charAt(i);
			} else {
				A.push(S.charAt(i));
			}
		}

		while (A.size() > 0) {
			B += A.peek();
			A.pop();
		}

		return B;
	}

	public static void main (String[] args) throws IOException {
                      // Your code here
		//Scanner input = new Scanner(System.in);
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String S = br.readLine();
		//String S = input.next();
		System.out.print(stringStack(S));
	}
}
*/