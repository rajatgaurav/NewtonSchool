/*
Problem Statement
Given an integer N, the task is to find the number of divisors of N which are divisible by 2.
Input
The input line contains T, denoting the number of testcases. First line of each testcase contains integer N 

Constraints: 
1 <= T <= 50 
1 <= N <= 10^9
Output
For each testcase in new line, you need to print the number of divisors of N which are exactly divisble by 2
Example
Input: 
2 
9 
8 

Output 
0 
3
*/

