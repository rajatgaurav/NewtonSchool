/*
Problem Statement
Given two sorted arrays A and B of size n and m respectively, return the median of the two sorted arrays. 
The overall run time complexity should be O(log (m+n))
Input
First line of input contains n, m the length of array A and B. 
Next two lines contains input of array A and B. 

Constraints 
1 <= n, m <= 1000 
-1e6 <= A[i], B[i] <= 1e6
Output
Print the median of two sorted arrays upto two decimal places.
Example
Sample Input : 
2 1 
1 3 
2 

Sample Output : 
2.00
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
    public static void main(String[] args) {
        // Your code here
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();
        int[] arr1 = new int[n];
        int m = input.nextInt();
        int[] arr2 = new int[m];
        
		for(int i = 0; i < n; i++) arr1[i] = input.nextInt();
        
		for(int i = 0; i < m; i++) arr2[i] = input.nextInt();
        
		System.out.printf("%.2f",getMedian(arr1, arr2, n, m));
        input.close();
    }

    static float getMedian(int arr1[], int arr2[], int n, int m){

        int i = 0, j = 0;
        float median = -1; 
        float median2 = -1;
        
		if((n + m) % 2 == 1){
        	int count = (n + m) / 2 + 1;
            while(i < n && j < m && count > 0){
                if(arr1[i] < arr2[j]){
                    median = arr1[i];
                    count--;
                    i++;
                }
				else{
                    median = arr2[j];
                    count--;
                    j++;
                }
            }
            while(i < n && count > 0){
                median = arr1[i];
                i++;
                count--;
            }
            while(j < m && count > 0) {
                median = arr2[j];
                j++;
                count--;
            }
        }
		else{
            int count = (n + m) / 2 + 1;
            while(i < n && j < m && count > 0){
                median2 = median;
                
				if(arr1[i] < arr2[j]){
                    median = arr1[i];
                    count--;
                    i++;
                }
				else{
                    median = arr2[j];
                    count--;
                    j++;
                }
            }
            while(i < n && count > 0){
                median2 = median;
                median = arr1[i];
                i++;
                count--;
            }
            while(j < m && count > 0) {
                median2 = median;
                median = arr2[j];
                j++;
                count--;
            }
            median = (median + median2) / 2;
        }
        return median;
    }
}