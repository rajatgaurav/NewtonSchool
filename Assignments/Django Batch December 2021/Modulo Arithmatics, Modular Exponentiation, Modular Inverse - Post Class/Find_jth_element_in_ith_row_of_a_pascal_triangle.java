/*
Problem Statement
In a pascal triangle starting from top to bottom. Find the value of the element in a given row and column position i.e. ith row and at jth position.

Input
The first line of input contains an integer T denoting the number of test cases. Each test case will have two integers indexes of the ith row and jth column provided in the new line. 

Constraints: 
1 <= T <= 100 
0 <= j <= i <= 500 
Output
Print the answer for each testcase in separated line. Since the output can be very large, mod your output by 10^9+7.
Example
Input: 
2 
0 0 
2 1 

Output: 
1 
2 
Explanation: The Pascal Triangle has first value as 1. (0th row 0th element). The pascal triangle has next set of values as 1 1. (1th row). On the next level, pascal triangle has values 1 2 1. (2nd row). Its 1st element is 2. (0 based).
*/

