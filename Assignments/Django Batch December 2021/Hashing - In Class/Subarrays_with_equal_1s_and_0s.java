/*
Problem Statement
Given an array arr[] of size N containing 0s and 1s only. The task is to count the subarrays having an equal number of 0s and 1s.
Input
The first line of the input contains an integer N denoting the size of the array and the second line contains N space-separated 0s and 1s. 

Constraints:- 
1 <= N <= 10^6 
0 <= A[i] <= 1
Output
For each test case, print the count of required sub-arrays in new line.
Example
Sample Input 
7 
1 0 0 1 0 1 1 

Sample Output 
8 

The index range for the 8 sub-arrays are: 
(0, 1), (2, 3), (0, 3), (3, 4), (4, 5) 
(2, 5), (0, 5), (1, 6)
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
		Scanner input = new Scanner(System.in);
		int n = input.nextInt();
		int[] arr = new int[n];
		for(int i = 0;i< n;i++){
			arr[i] = input.nextInt();
		}
		System.out.println(countSubarrays(arr, n));
	}

	private static long countSubarrays(int[] arr, int n){
		for(int i = 0;i< n;i++){
			if(arr[i] == 0){
				arr[i] = -1;
			}
		}

		long result = 0;
		long cumSum = 0;
		HashMap<Long, Integer> map = new HashMap<>();

		for(int i = 0;i< n;i++){
			cumSum += arr[i];
			if(cumSum == 0){
				result++;
			}

			if(map.containsKey(cumSum)){
				result += map.get(cumSum);
				map.put(cumSum, map.get(cumSum) + 1);
			}else{
				map.put(cumSum, 1);
			}
		}

		return result;
	}
}