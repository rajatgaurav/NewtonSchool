/*
Problem Statement
Given a string s, your task is to divide the string in as many parts as it is possible such that a character can only be present in at most 1 part.
Input
Input contains a single string S. 

Constraints:- 
1 < = N < = 100000 

Note:- 
String will contain only lowercase english alphabets
Output
Print the maximum number of partitions.
Example
Sample Input:- 
abcdaefghgheklmnol 

Sample Output:- 
4 

Explanation:- 
abcda efghghe k lmnol 

Sample Input:- 
newtonschool 

Sample Output:- 
2 

Explanation:- 
newtonschoo l
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
		Scanner input = new Scanner(System.in);
		String str = input.nextLine();
        int[] lastPos = new int[26];
        
        for(int i = 0; i < str.length();i++) {
            lastPos[str.charAt(i) - 'a'] = i;
        }
        int partitions = 0;
        int ending = 0;
        
        for(int i = 0; i < str.length();i++) {
            ending = Math.max(ending, lastPos[str.charAt(i) - 'a']);
            if(i == ending) {
                partitions++;
            }
        }
        System.out.println(partitions);
    }
}