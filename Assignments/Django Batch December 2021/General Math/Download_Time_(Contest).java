/*
Problem Statement
Internet download speed is often expressed in bit per second whereas file size is expressed in Bytes. It is known that 1 Byte = 8 bits. Given file size in megabytes(MB) and internet speed in megabits per seconds(Mbps), find the time taken in seconds to download the file.
Input
The only line of input contains two integers denoting the file size in MB and download speed in Mbps. 

1 <= file size <= 1000 
1 <= download speed <= 1000
Output
Print a single integer denoting the time taken to download the file in seconds. 
It is guaranteed that the result will be an integer.
Example
Sample Input: 
10 16 

Sample Output: 
5
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework
//import javax.swing.JOptionPane;

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
		Scanner input = new Scanner(System.in);
		
        int sec = 0;
        int mb = input.nextInt();
		int mbps = input.nextInt();
        
        while (mb <= 1 && mbps <= 1) {              
            if (mb <= 1000 && mbps <= 1000) {
                mb = mb + 0;
                mbps = mbps + 0;
            }
        }
        sec = (mb*8)/mbps;   
        System.out.println(sec);
    }
}