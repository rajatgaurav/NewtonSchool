/*
Problem Statement
Doctor Strange stumbles upon a rather curious artefact while spending holidays in Egypt. An antique board game but it can only be unlocked by a magical die. The only clue on the box is to get a sum of B by throwing the die A times. Help Doctor Strange if it is possible to get the particular sum. Print “YES” if it is possible, otherwise print “NO”. 

Note:- Consider the die to be a regular one with face {1, 2, 3, 4, 5, 6}.
Input
The input contains two integers A and B separated by spaces. 

Constraints:- 
• 1≤A≤100 
• 1≤B≤1000 
A and B are integers.
Output
Print "YES" if it is possible to get a Sum of B by throwing die A times else print "NO".
Example
Sample Input:- 
10 100 

Sample Output:- 
NO 

Sample Input:- 
6 30 

Sample Output:- 
YES
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                
                Scanner input = new Scanner(System.in);
                int x = input.nextInt();
                int y = input.nextInt();

                int maxSum = x * 6;

                if(maxSum >= y && x <= y){
                    System.out.printf("YES");
                } else {
                    System.out.printf("NO");
                }
	}
}