/*
Problem Statement
Given an array A[], of size N containing positive integers. You need to print the elements of array in increasing order.
Input
First line of the input denotes the number of test cases 'T'. First line of the test case is the size of array and second line consists of array elements. 

For Python Users just complete the given function 

Constraints: 
1 <= T <= 100 
1 <= N <= 10^3 
1 <= A[i] <= 10^3
Output
For each testcase print the sorted array in a new line.
Example
Input: 
2 
5 
4 1 3 9 7 
10 
10 9 8 7 6 5 4 3 2 1 

Output: 
1 3 4 7 9 
1 2 3 4 5 6 7 8 9 10 

Explanation: 
Testcase 1: 1 3 4 7 9 are in sorted form. 
Testcase 2: For the given input , 1 2 3 4 5 6 7 8 9 10 are in sorted form.
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
		Scanner input = new Scanner(System.in);
		int t = input.nextInt();
		while(t>0){
			int n = input.nextInt();
			int[] arr = new int[n];
			for(int i =0;i<n;i++){
				arr[i] = input.nextInt();
			}
			bubbleSort(arr, n);
			System.out.println(" ");
			t--;
		}
	}

	private static void bubbleSort(int[] arr, int n){
		for(int i =0; i < n-1;i++){
			for(int j = 0; j < n -1;j++){
				if(arr[j] > arr[j+1]){
					swap(arr, j, j+1);
				}
			}
		}

		for(int i =0;i<n;i++){
			System.out.print(arr[i] + " ");
		}
	}

	private static void swap(int[] arr, int x, int y){
		int temp = arr[x];
		arr[x] = arr[y];
		arr[y] = temp;
	}
}