/*
Problem Statement
Given an array of N elements, for each element find the value of nearest element to the right which is having frequency greater than as that of current element. If there does not exist an answer for a position, then make the value ‘-1’.
Input
Firs line of input contains the size of the array N, next line contains N space separated integers depicting values of the array. 

Constraints:- 
1 < = N < = 100000 
1 < = Arr[i] < = 100000
Output
For each element print the value of nearest element to the right which is having frequency greater than as that of current element. If there does not exist an answer for a position, then make the value ‘-1’
Example
Sample Input:- 
6 
1 2 1 3 2 1 

Sample Output:- 
-1 1 -1 2 1 -1 

Sample Input:- 
6 
1 2 2 3 3 3 

Sample Output:- 
2 3 3 -1 -1 -1
*/

import java.io.*; // for handling input/output
import java.util.*; // contains Collections framework

// don't change the name of this class
// you can add inner classes if needed
class Main {
	public static void main (String[] args) {
                      // Your code here
		Scanner input = new Scanner(System.in);
		int n = input.nextInt();
		int[] arr = new int[n];
		HashMap<Integer, Integer> freq = new HashMap<>();
		for(int i =0;i< n;i++){
			arr[i]= input.nextInt();
			
			if(freq.containsKey(arr[i])){
				freq.put(arr[i], freq.get(arr[i]) + 1);
			}else{
				freq.put(arr[i], 1);
			}
		}
		printNGF(arr, n, freq);
	}

	private static void printNGF(int[] arr, int n, HashMap<Integer, Integer> freq){
		int[] result = new int[n];
		Stack<Integer> stack = new Stack<>();

		stack.push(0);

		for(int i =1;i< n;i++){

			if(freq.get(arr[stack.peek()]) > freq.get(arr[i])){
				stack.push(i);
			}else{

				while(!stack.isEmpty() && freq.get(arr[stack.peek()]) < freq.get(arr[i])){
					result[stack.peek()] = arr[i];
					stack.pop();
				}

				stack.push(i);

			}
		}

		while(!stack.isEmpty()){
			result[stack.peek()] = -1;
			stack.pop();
		}

		for(int i = 0;i<n;i++){
			System.out.print(result[i] + " ");
		}
	}
}